# Exigences 3D Viewer

## 1. Charger un modèle 3D

- Quoi ? : Un modèle 3D au format `.ply`.
- Qui ? : L'utilisateur qui à l'application hôte (avec la scène) ouverte.
- Où ? : Dans la scène qui est actuellement ouverte.
- Quand ? : Lorsque que l'utilisateur le veut.
- Comment ? : Grace à un menu toolstrip.
- Combien ? : Il peut en charger que un à la fois, mais il peut y en avoir plusieurs sur une scène.
- Pourquoi ? : Pour pouvoir le voir sur les différents afficheurs.

## 2. Sauvegarder et charger une scène

- Quoi ? : Une scène stockée dans un format que j'aurais créer.
- Qui ? : L'utilisateur qui à l'application hôte (avec la scène) ouverte.
- Où ? : Sur le disque dur de l'utilisateur.
- Quand ? : Quand l'utilisateur à envie de charger ou de sauvegarder une scène.
- Comment ? : En appuyant sur `ctrl+s` ou dans un menu toolstrip.
- Combien ? : L'utilisateur ne peut charger qu'une seule scène à la fois, mais il peut sauvegarder le nombre qu'il veut.
- Pourquoi ? : Afin de conserver la disposition des objets dans la scène.

## 3. Pouvoir déplacer les objets sur la scène

- Quoi ? : Les objets et lumières qui sont déjà chargée sur la scène.
- Qui ? : L'utilisateur qui à l'application hôte (avec la scène) ouverte.
- Où ? : N'importe ou l'utilisateur le souhaite.
- Quand ? : Quand l'utilisateur le veut.
- Comment ? : À l'aide d'un menu dans l'application qui permet de modifier la position et la rotation de l'objet.
- Combien ? : On peut déplacer un objet à la fois, mais tous les objets peuvent être déplacé, même la caméra.
- Pourquoi ? : Afin de pouvoir positionner les objets comme on le souhaite.

## 4. Pouvoir rendre la scène sur des afficheurs distants

- Quoi ? : La scène rendue, donc "l'image" 2D.
- Qui ? : Le programme hôte.
- Où ? : Dans les afficheurs qui "s'inscrivent" à l'affichage du programme hôte.
- Quand ? : Dès que la scène est mise à jour.
- Comment ? : Avec un protocole standardisé.
- Combien ? : Il peut y avoir autant d'afficheurs que la machine hôte peut rendre.
- Pourquoi ? : Affin de pouvoir afficher la scène sur n'importe quoi et montrer la maitrise de l'affichage 3D.

## 5. Avoir une liste des objets sur la scène

- Quoi ? : Une liste des objets qui sont actuellement sur la scène.
- Qui ? : L'utilisateur qui à l'application hôte.
- Où ? : À côté de la scène.
- Quand ? : Tout le temps.
- Comment ? : Dans une listbox.
- Combien ? : Une seule.
- Pourquoi ? : Afin de pouvoir supprimer l'objet sélectionner de cette liste et de pouvoir quels objets sont actuellement sur la scène.

## 6. Pouvoir ajouter une lumière

- Quoi ? : Une lumière globale (soleil).
- Qui ? : L'utilisateur avec l'application hôte.
- Où ? : Dans la scène qui est actuellement ouverte.
- Quand ? : Quand l'utilisateur veut ajouter une lumière.
- Comment ? : Avec un menu menu toolstrip.
- Combien ? : Il peut ajouter le nombre de lumière qu'il veut.
- Pourquoi ? : Afin de pouvoir éclairer les objets sur la scène.

## 7. Pouvoir pivoter les objets sur la scène

- Quoi ? : Un objet présent sur la scène.
- Qui ? : L'utilisateur qui veut faire pivoter cet objet.
- Où ? : Dans la scène, autour de son origine (le 0, 0, 0 local de l'objet).
- Quand ? : Quand l'utilisateur à envie d'e faire pivoter l'objet
- Comment ? : Grace à un menu qui permet de controller la rotation X, Y et Z de l'objet.
- Combien ? : Il peut faire pivoter qu'un seul objet à la fois mais il peut en faire pivoter tous les objets.
- Pourquoi ? : Afin de pouvoir voir les objets de tous les côtés.

## 8. Pouvoir effectuer une homothétie sur les objets de la scène

- Quoi ? : Un objet présent sur la scène.
- Qui ? : L'utilisateur qui veut faire un homothétie sur cet objet.
- Où ? : Dans la scène et autour de son origine (le 0, 0, 0 local de l'objet).
- Quand ? : Quand l'utilisateur à envie d'e faire pivoter l'objet
- Comment ? : Grace à un menu qui permet de controller la rotation X, Y et Z de l'objet.
- Combien ? : Il peut faire pivoter qu'un seul objet à la fois mais il peut en faire pivoter tous les objets.
- Pourquoi ? : Afin de pouvoir voir les objets de tous les côtés.

## 9. Pouvoir placer une caméra sur la scène qui vas rendre les objets de son point de vue

- Quoi ? : Une caméra.
- Qui ? : L'utilisateur qui veut placer la caméra.
- Où ? : Dans la scène et n'importe ou dans celle-ci.
- Quand ? : Quand l'utilisateur veut ajouter une caméra.
- Comment ? : Grace à un menu qui permet d'ajouter cette caméra.
- Combien ? : Le nombre de caméra que l'utilisateur veut, mais il n'y a qu'une seule caméra qui peut rendre à la fois.
- Pourquoi ? : Afin de pouvoir voir les objets de différents angles.
# Jalon technique 1

## Technologies

- **Language** : C#
- **Framework** : .NET 5.0
- **IDE** : Visual Studio 2019

## Travail effectué

- Struct vecteurs (`Vector2`, `Vector3`)
- Début du struct `Matrix3`

## Travail à effectuer

- Fin du struct `Matrix3`
- Structs `Vector4` et `Matrix4` ?
- Projections de points 3D sur un plan 2D
- Affichage de faces
- Création d'une caméra (qui peut être bougée dans l'espace)
- Éclairer les faces en fonction de leur orientation par rapport à une source de lumière (normale)
- Charger un modèle 3D au format `.ply`

Pour ces points, je ne suis pas encore sur si je vais les effectuer :

- Affichage de la projection sur des afficheurs quelconque avec un protocole créé pour ça (sur afficher matriciel)
- Envoyer les informations de ces afficheurs par le réseau
- Pouvoir afficher sur plusieurs afficheurs de différentes résolutions en même temps

## Difficultés techniques

- Il faut les connaissances suffisantes en math
- Tout le projet dépend des classes mathématiques (elles doivent donc bien être réalisée)
- Comme je n'ai jamais fait de réseau de C#, il faut que je me renseigne sur le sujet
- Pour pouvoir afficher avec un framerate suffisant je vais devoir bien optimiser mon code
- Avoir une bonne gestion du temps en windows form est compliqué

# Cahier des charges

## Titre

3D Viewer

## Objectif

Il existe beaucoup de programmes qui permettent d’afficher des modèles 3D. Cela dit, j’aimerais beaucoup explorer l’aspect mathématique de la projection d’objets 3D sur un plan 2D. Dans le cadre de ce projet, j’effectuerai une application qui permettra d'afficher des modèles 3D sur n'importe quel afficheur matriciel. 

## Matériel et logiciels

- Ordinateur de l’école
- Deux écrans
- Windows 10 
- Visual Studio 2019
- C#
- .NET Core
- Notion
- Git (hébergé sur GitLab)

## Prérequis

- Connaissances en calcul matriciel
- Savoir faire de la POO en C#

## Descriptif complet du projet

### Planification

Voir annexe "To-Do_3D_Viewer.pdf".
C'est un export d'une page [notion](https://www.notion.so/c058f1819beb4374a0ffc04747b6dcbb?v=4f9c789008ea4481999ef286d4b10838).

La page «To-Do 3D Viewer» peut être visualisée sur une timeline en cliquant sur «By Status» puis en sélectionnant «Timeline View».

![Notion_AxcyXT2w6n](.\Notion_AxcyXT2w6n.png)

### Mindmap

![Travail de diplôme - 3D Viewer@2x(3)](.\Travail de diplôme - 3D Viewer@2x(3).png)

### Méthodologie

Pour ce projet, je vais utiliser la méthodologie *Scrum*. Notre projet étant séparé par des évaluations intermédiaire, je les utiliserais comme fin de sprint de ce projet. 

### Description de l'application

3D Viewer sera capable de charger des modèles 3D dans une scène. Cette scène pourra être sauvegardée et chargée. Il sera également possible de rendre cette scène sur n'importe quel afficheur à l'aide d'un protocole de communication standardisé. Il sera par exemple possible d'afficher le modèle 3D sur une application console ou bien même Windows Form, du temps qu'elle lisent les informations envoyée par le réseau.

## Livrables

- ​	Mindmap
- ​	Planning
- ​	Rapport de projet
- ​	Manuel utilisateur (si applicable)
- ​	Journal de travail ou Log Book
- ​	Poster
- ​	Résumé / Abstract


# Journal de bord pré-POC 3D Viewer

## Struct `Vector2`

La première question que je me suis posée lors de la création du struct `Vector2` était de savoir si j'allais utiliser des `double` ou des `floats` pour stocker les valeurs.
J'ai choisi de partir sur des `double` car ils permettent une plus grande précision et que le programme allait tourner sur une machine 64 bits.

J'avais déjà créé un struct vecteur en [Beef](https://www.beeflang.org/) avec les mêmes éléments que le struct vecteur présent sur [Unity](https://docs.unity3d.com/ScriptReference/Vector2.html).
Mais pour être sûr que je faisais les choses bien, j'ai regardé le tutoriel "[A Vector Type for C#](https://www.codeproject.com/articles/17425/a-vector-type-for-c)".

Une question qu'on peut se poser en créant un vecteur est s'il faut intégrer de la tolérance dans la comparaison des doubles dans notre struct.
J'en ai conclu qu'il ne le faut pas, car cette valeur dépend du type de projet, ce que je n'avais pas fait dans mon struct en `Beef` d'ailleurs.

La comparaison des différents vecteurs se fait avec la propriété calculée `SqrMagnitude` car elle ne fait pas `Math.Sqrt()` dedans ce qui la rend plus performante que juste `Magnitude`. Malgré le fait qu'il n'y ait pas de tolérance intégrée au code du vecteur, il y a quand même des méthodes qui permettent de comparer les vecteurs en précisant une tolérance en paramètre.

Il y a plusieurs conditions pour que la normalisation d'un vecteur soit possible. Ce qui fait que j'ai fait deux méthodes, une qui retourne une exception lorsque les valeurs ne sont pas bonnes et une qui retourne un vecteur par défaut. C'est ensuite au développeur qui utilise la classe de choisir quelle implémentation il veut utiliser.

Je ne sais pas si la notion de "Back-face" fait sens en 2D, je n'ai donc pas implémenté de méthode pour ceci dans mon struct `Vector2`. L'article sur `codeproject.com` parle également d'interpolation et d'extrapolation, ce que je n'ai pas compris pour l'instant, je me documenterais plus tard sur ce que c'est et si je devrais l'implémenter. Je n'ai également pas implémenté la projection , la réjection, la réflexion, la rotation  dans le `Vector2`.

Pour les méthodes `PowComponents`, `SqrtComponents`, etc., l'article crée un nouvel objet dans la méthode statique, mais l'applique à l'objet de base dans la méthode non statique, ce qui m'a étonné, car le struct est censé être immutable comme dit au début. J'ai donc décidé de créer un nouvel objet.

## Struct `Vector3`

Je ne savais pas comment aborder la création de ce struct, je me disais que je devais peut-être faire de l'héritage avec le `Vector2` mais cela n'est pas possible. J'ai donc fini par copier-coller le code de mon vecteur 2D dans le vecteur 3D et j'ai ensuite adapté le code.

J'ai ensuite intégré le code pour l'interpolation et l'extrapolation. J'ai fait de même pour le back-face, mixed product, la projection et la réjection.

J'ai fini le struct en implémentant la rotation du vecteur dans tous les axes.

## Struct `Matrix3`

J'ai commencé par me renseigner sur comment aborder ce struct, je ne savais pas si je devais en faire un struct ou une classe, donc j'ai regardé dans des librairies que je connais :

- https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.matrix?view=net-5.0
- https://github.com/opentk/opentk/blob/master/src/OpenTK.Mathematics/Matrix/Matrix3.cs

Étant donné que les deux exemples ci-dessus sont des struct, j'ai décidé d'en faire un struct (ce qui est également cohérent avec les vecteurs que j'ai faits précédemment)

J'ai ensuite regardé des bouts de codes pour voir ce que je devrais mettre dans le struct :

- https://codes-sources.commentcamarche.net/source/34171-classe-matrice
- https://www.csharpstar.com/create-matrix-and-different-matrix-operations-in-csharp/
- https://medium.com/@furkanicus/how-to-create-a-matrix-class-using-c-3641f37809c7

Étant donné que mes vecteurs sont immutables, je me suis demandé si je devais faire la matrice immutable également. J'ai donc lu ce post sur stack overflow :

- https://codereview.stackexchange.com/questions/64474/immutable-matrix

En voyant cet exemple il me semble OK de rendre la matrice immutable. 

Je me suis ensuite demandé comment je stockerais les données de la matrice, soit avec un tableau de double à deux dimensions, soit avec des vecteurs comment dans le code de OpenTK. J'ai choisir de partir sur un tableau à deux dimensions de taille fixe.

J'ai implémenté les différents opérateurs pour les opérations scalaires sur les matrices.
J'ai ensuite implémenté des méthodes qui se trouvent dans la classe de `OpenTK`. Je n'ai cependant pas encore implémenté l'inversion, car je ne comprends pas encore vraiment bien comment ce code fonctionne.

## Struct `Matrix4`

M. Bonvin m'a prévenu que les matrices 3 et les vecteurs 3 ne permettent pas d'avoir de translation, j'ai donc arrêté de développer mon struct `Matrix3` pour me concentrer sur un struct `Matrix4` qui permettra de faire toutes les opérations mathématiques que j'aimerais faire.
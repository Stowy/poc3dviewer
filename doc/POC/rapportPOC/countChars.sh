#!/bin/bash
# detex $1.tex | awk NF | wc -m
pdftotext "$1.pdf" && cat "$1.txt" | awk NF | wc -m && rm "$1.txt"
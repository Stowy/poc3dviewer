# Logbook POC 3D Viewer

## 30/03/2021

Matin : Changement de classe et réinstallation de Windows 10.

Après-midi : Refactorisation du struct Vector4 et Matrix4

Je suis un peu confus sur comment gérer ce Vecteur 4, car il ne représente pas tellement un point dans un espace 4D mais un point dans un espace 3D avec des coordonnées homogènes. Par exemple si je soustrait deux Vecteurs 4, est-ce que la valeur W devrait également être soustraite ? 

Pour l'instant je ne travaille qu'avec des Matrix4 pour simplifier mon travail je vais donc mettre de côté ce problème avec le Vecteur 4. Je vais considérer la matrice comme représentant une face pour l'instant.

J'ai réussi à afficher un triangle avec une projection orthographique.

TODO : Faire pivoter cette face avec une matrice de rotation.

## 31/03/2021

Création des matrices de rotation sur les axes X, Y et Z.

Modification de `Vector3` et `Vector4` afin de mettre la face dans ceux-ci.

Ajout d'un moyen de convertir un Vecteur 4 qui contient des coordonnées homogènes en Vecteur 3 qui contient les coordonnées physiques de celui-ci.

Ajout de pouvoir visualiser la face en fil de fer ou remplie.

J'ai ensuite mis un cube que j'ai hard codé dans une liste 2d de Vecteur4 pour voir si mon code de visualisation marche vraiment (difficile à dire avec une seule face).

Avec le cube, je me suis aperçue que mon backface culling ne marchait pas. J'imagine que c'est dû à l'ordre dans lequel je déclare les sommets des faces de mon cube et que du coup la normale que je calcule n'est pas dans le bon sens.

Commencé la doc.

Comme je fait ma doc en `LaTeX`, j'ai fait un script afin de compter le nombre  de charactères de celle-ci. D'abord j'enlève les charactères `LaTeX` avec `detex` puis j'utilise `awk`pour enlever les retours à la ligne vides et les espaces qui se trouvent à la suite puis je compte le nombre de charactère avec `wc`.

```bash
detex rapportPoc.tex | awk NF | wc -m
```

## 06/04/2021

Pour essayer de régler mon problème de backface culling, j'ai essayé de mieux organiser mes faces, j'ai donc décidé de créer un stuct `Face` qui contient une liste de Vector4. J'ai ensuite pris la façon dont le cube est déclaré dans un ply et l'ai mis dans mon code, comme ça je suis sur que mes faces sont bien déclarées. Après avoir adapté mon code d'affichage du cube, j'ai pu le tester, et ça ne marchait toujours pas. Avec le truc spécial en plus où il y a un triangle dans le centre du cube, je ne sais pas ce qui cause ceci.

## 07/04/2021

Pour essayer de mieux voir ce qui se passe mal dans mon backface culling, j'ai voulu créer une matrice de projection perspective. Pour ce faire, j'ai utilisé le code d'`OpenTK`, comme je n'ai pas totalement compris comment faire ça avec ce qui se trouve dans le livre. Malheureusement, l'utilisation de cette matrice ne marche pas et je ne sais pas ce qui ne marche pas.

![perspective](.\perspective.png)

Alors qu'avec la matrice orthographique ça marche bien.

![orthographic](.\orthographic.png)

J'ai essayé de dessiner les normales des faces pour les voir, mais je n'ai pas réussi.

TODO : Finir le dessin des normales.

## 10/04/2021

J'ai fini de dessiner les normales, et je pense que déjà un des problèmes que j'avais avec les normales c'est qu'on les reçoit relativement à la face, il faut donc faire attention à ça quand on veut regarder si elles font face à la caméra. Aussi, je ne vois pas de fautes dans mon code pour dessiner les normales, je suppose donc que le problème vient soit de comment je les calcule, soit de comment je déclare les faces de mon cube ce qui fait que les normales ne sont pas bien calculées.

![badNormals](.\badNormals.png)

Il y a 2 normales sur cette image qui sont bien positionnées, mais les autres font n'importe quoi. Je n'en ai que 4 alors que je devrais en avoir 6. Aussi, il y en a deux qui sont en diagonales, mais je n'ai aucune idée de pourquoi.

Mon code pour calculer la normale ressemble à ça :

```c#
public Vector3 Normal => vertices[0].ToPhysicalCoords().Cross(vertices[1].ToPhysicalCoords());
```

Je prends deux sommets de la face que je convertis en coordonnées physiques (donc en `Vector3`) comme ce sont des coordonnées homogènes. Je fais ensuite le produit en croix de ces deux vecteurs ce qui est censé me retourner la normale de cette face comme ce calcul retourne un vecteur perpendiculaire aux deux autres. Mais je ne suis pas sur de l'ordre des sommets que j'utilise, j'imagine que ça impacte la direction de la normale. Aussi, j'ai peut-être mal déclaré mon cube, ce qui fait que quand je fais ces calculs de normales, il se passe n'importe quoi. J'ai pourtant copié les données qui se trouvaient dans un exemple de fichier `.ply` qui représente un cube.

En essayant de normaliser les normales, j'ai reçu un message d'erreur disant que j'avais des normales qui étaient à 0,0,0. J'imagine que c'est pour ça que je ne vois que 4 normales.

Je suis tombé sur cet article `StackOverflow` : https://stackoverflow.com/questions/40454789/computing-face-normals-and-winding et je n'ai pas fait la soustraction des deux vecteurs. Je ne sais pas a quoi ça sert mais du coup je l'ai implémenté dans mon code : 

```c#
public Vector3 Normal
{
    get
    {
        Vector3 v0 = vertices[0].ToPhysicalCoords() - vertices[2].ToPhysicalCoords();
        Vector3 v1 = vertices[1].ToPhysicalCoords() - vertices[2].ToPhysicalCoords();
        return v0.Cross(v1);
    }
}
```

Mais cela ne règle pas mes problèmes de normales.

![badNormals2](.\badNormals2.png)

Et quand je normalise la normale, c'est encore plus bizarre.

![normalizedNormals](.\normalizedNormals.png)

On voit les normales qui j'imagine étaient proche 0 avant. Les normales bougent bizarrement quand le cube tourne. Elles ne restent pas dans la direction où elles sont censées rester.

## 12/04/2021

J'ai essayé d'afficher une seule face pour voir si le calcul des normales marche et ça semble être le cas.

![niceNormal](.\niceNormal.png)

Et normalisé ça semble aussi marcher.

![niceNormalized](.\niceNormalNormalized.png)

Je pense qu'il est donc possible d'en déduire que c'est surement la déclaration du cube qui est mauvaise.

Ma théorie c'est que le fichier que j'ai pris fait des faces à 4 sommets. Je vais essayer avec un cube constitué de triangles et non pas de carrés.

Mais ça ne marche pas non plus.

![badNormals2Comeback](.\badNormals2Comeback.png)

J'ai pourtant bien fait attention à cette remarque dans la spécifiquation du format `.ply` :

> Note that the face list generates triangles in the order of a  TRIANGLE FAN, not a TRIANGLE STRIP. In the example above, the first face
>   4 0 1 2 3 
>  Is composed of the triangles 0,1,2 and 0,2,3 and not 0,1,2 and 1,2,3.

Ce qui me donne cette déclaration pour le cube :

```C#
Vector4[] vertices = new Vector4[8]
{
    new(0, 0, 0, 1),
    new(0, 0, 1, 1),
    new(0, 1, 1, 1),
    new(0, 1, 0, 1),
    new(1, 0, 0, 1),
    new(1, 0, 1, 1),
    new(1, 1, 1, 1),
    new(1, 1, 0, 1),
};

cube = new Face[12]
{
    new(new Vector4[3] { vertices[0], vertices[1], vertices[2] }),
    new(new Vector4[3] { vertices[0], vertices[2], vertices[3] }),
    new(new Vector4[3] { vertices[7], vertices[6], vertices[5] }),
    new(new Vector4[3] { vertices[7], vertices[5], vertices[4] }),
    new(new Vector4[3] { vertices[0], vertices[4], vertices[5] }),
    new(new Vector4[3] { vertices[0], vertices[5], vertices[1] }),
    new(new Vector4[3] { vertices[1], vertices[5], vertices[6] }),
    new(new Vector4[3] { vertices[1], vertices[6], vertices[2] }),
    new(new Vector4[3] { vertices[2], vertices[6], vertices[7] }),
    new(new Vector4[3] { vertices[2], vertices[7], vertices[3] }),
    new(new Vector4[3] { vertices[3], vertices[7], vertices[4] }),
    new(new Vector4[3] { vertices[3], vertices[4], vertices[0] }),
};

```

En partant de ça dans le `.ply` :

```
0 0 0                      { start of vertex list }
0 0 1
0 1 1
0 1 0
1 0 0
1 0 1
1 1 1
1 1 0
4 0 1 2 3                  { start of face list }
4 7 6 5 4
4 0 4 5 1
4 1 5 6 2
4 2 6 7 3
4 3 7 4 0
```

J'ai avancé la docs après avoir eu plus d'information de la part de M. Garcia sur ce qu'il faut mettre dedans.

J'ai mis à jour la commande que j'utilise pour compter le nombre de charactères de mon document LaTeX :

```bash
pdftotext "$1.pdf" && cat "$1.txt" | awk NF | wc -m && rm "$1.txt"
```

Elle convertit le PDF en texte, enlève les retours à la ligne vides  avec awk, compte le nombre de charactères avec wc puis supprime le  fichier texte.

## 13/04/2021

Avancé le rapport.

En faisant des test sur mon code, je pense savoir d'ou vient le problème, mon code pour savoir si la normale fait face à la caméra est surment faux. J'utilise un bout de code qui demande en entré la ligne de mire de la caméra, sauf que avec ma caméra orthographique, je ne savait pas quoi mettre alors j'ai juste mis une valeur qui me paraissait correcte.

Après avoir posé une question à M. Bonvin, il m'as demandé de faire en sorte d'avoir 3 vues pour les 3 axes de projection orthographiques. Mais je me rends compte que les axes X et Y ne marchent pas.

![badXandY](.\badXandY.png)

Je me suis rendu compte que je prenais toujours les valeurs X et Y de mes vecteurs lors du dessin, alors qu'il faut que je prenne les valeurs Y et Z pour une projection sur l'axe X, les valeurs X et Z pour une projection sur l'axe Y alors que les valeurs X et Y vont bien pour une projection sur l'axe Z.

![niceOrtho](.\niceOrtho.png)

Cela dit, cela n'as pas règlé mon problème de normales.

![normals2RetourDuComeback](.\badNormals2RetourDuComeback.png)

Il me semble que absolument toutes les normales du cube sont en diagonales. Mais lorsque que j'affiche une seule face c'est encore plus bizarre. On dirait que pendant la rotation de la face, la normale "s'accroche" à un point puis change de point pour passer à un autre juste après.

![wierdNormal](.\wierdNormal.gif)

## 14/04/2021

Je pense que mon problème vient principalement sur comment je visualise mes normales. J'ai donc décidé d'arrêter de perdre du temps sur ce sujet et de me concentrer sur le fait de faire marcher le backface culling.

Après avoir fait en sorte de pouvoir obtenir la ligne de mire de chaque vue du modèle, j'ai testé si le backface culling marche et j'ai eu ce résultat :

![backfaceCullingVeryNice](.\backfaceCullingVeryNice.png)

On peut voir que le backface culling marche pour l'axe X et l'axe Y mais pas pour l'axe Z. Je ne sais pas pourquoi, j'imagine que c'est une histoire de ligne de mire.

En changeant la ligne de mire à l'axe Y, pour la projection Z, je vois que le résultat est cohérent, juste que la caméra n'est pas bien placée, le problème survient donc quand la ligne de mire est égale à (0, 0, 1). Voici le code que j'ai changé :

```C#
Vector3 lineOfSight = axis switch
{
    Axis.X => Vector3.XAxis,
    Axis.Y => Vector3.YAxis,
    _ => Vector3.YAxis,
};
```

Malgré ce bug restant, M. Bonvin m'as dit d'arrêter d'avancer sur le code, car ce que j'avais produit était suffisant et m'as conseillé de me concentrer sur la doc.

## 15/04/2021

J'ai fini le rapport.
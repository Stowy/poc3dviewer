﻿namespace POC3DViewer
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;
    using POC3DViewer.Mathematics;

    /// <summary>
    /// Main form of the app.
    /// </summary>
    public partial class Form1 : Form
    {
        private const double DrawOffset = 150;
        private const double ScaleFactor = 120;

        private const double IncrementAngleX = 0.01d;
        private const double IncrementAngleY = 0.005d;
        private const double IncrementAngleZ = 0.0075d;

        private const int Fps = 999;

        private readonly Face[] cube;
        private readonly List<Face> transformedCube;

        private double angleX;
        private double angleY;
        private double angleZ;

        private bool isFilled;

        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            Vector4[] vertices = new Vector4[8]
            {
                new(-0.5d, -0.5d, -0.5d, 1d),
                new(-0.5d, -0.5d,  0.5d, 1d),
                new(-0.5d,  0.5d,  0.5d, 1d),
                new(-0.5d,  0.5d, -0.5d, 1d),
                new(0.5d,  -0.5d, -0.5d, 1d),
                new(0.5d,  -0.5d,  0.5d, 1d),
                new(0.5d,   0.5d,  0.5d, 1d),
                new(0.5d,   0.5d, -0.5d, 1d),
            };

            //cube = new Face[]
            //{
            //     new(new Vector4[3] { vertices[0], vertices[1], vertices[2] }),
            //     new(new Vector4[3] { vertices[0], vertices[2], vertices[3] }),
            //     new(new Vector4[3] { vertices[7], vertices[6], vertices[5] }),
            //     new(new Vector4[3] { vertices[7], vertices[5], vertices[4] }),
            //     new(new Vector4[3] { vertices[0], vertices[4], vertices[5] }),
            //     new(new Vector4[3] { vertices[0], vertices[5], vertices[1] }),
            //     new(new Vector4[3] { vertices[1], vertices[5], vertices[6] }),
            //     new(new Vector4[3] { vertices[1], vertices[6], vertices[2] }),
            //     new(new Vector4[3] { vertices[2], vertices[6], vertices[7] }),
            //     new(new Vector4[3] { vertices[2], vertices[7], vertices[3] }),
            //     new(new Vector4[3] { vertices[3], vertices[7], vertices[4] }),
            //     new(new Vector4[3] { vertices[3], vertices[4], vertices[0] }),
            //};

            cube = new Face[6]
            {
                new(new Vector4[4] { vertices[0], vertices[1], vertices[2], vertices[3] }),
                new(new Vector4[4] { vertices[7], vertices[6], vertices[5], vertices[4] }),
                new(new Vector4[4] { vertices[0], vertices[4], vertices[5], vertices[1] }),
                new(new Vector4[4] { vertices[1], vertices[5], vertices[6], vertices[2] }),
                new(new Vector4[4] { vertices[2], vertices[6], vertices[7], vertices[3] }),
                new(new Vector4[4] { vertices[3], vertices[7], vertices[4], vertices[0] }),
            };

            DoubleBuffered = true;
            fpsTimer.Interval = 1000 / Fps;
            transformedCube = new();
        }

        private enum Axis
        {
            X,
            Y,
            Z,
        }

        private void Form1_Load(object sender, System.EventArgs e)
        {
        }

        private void PaintCube(PaintEventArgs e, Matrix4 projection, Axis axis)
        {
            e.Graphics.Clear(Color.Black);

            // Sort the faces to show
            List<Face> facesToShow = new();

            foreach (Face face in transformedCube)
            {
                if (chbBackface.Checked)
                {
                    Vector3 normal = face.Normal;
                    Vector3 lineOfSight = axis switch
                    {
                        Axis.X => Vector3.XAxis,
                        Axis.Y => Vector3.YAxis,
                        _ => Vector3.ZAxis,
                    };

                    if (!normal.IsBackFace(lineOfSight))
                    {
                        facesToShow.Add(face);
                    }
                }
                else
                {
                    facesToShow.Add(face);
                }
            }

            // Show the faces
            facesToShow.ForEach((face) =>
            {
                Face projected = face * projection;
                Vector3[] physical = projected.ToPhysicalCoords();

                DrawFace(physical, axis, e);
            });
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e) => PaintCube(e, Matrix4.ProjectionOrthoX, Axis.X);

        private void PictureBox2_Paint(object sender, PaintEventArgs e) => PaintCube(e, Matrix4.ProjectionOrthoY, Axis.Y);

        private void PictureBox3_Paint(object sender, PaintEventArgs e) => PaintCube(e, Matrix4.ProjectionOrthoZ, Axis.Z);

        private void DrawFace(Vector3[] points, Axis axis, PaintEventArgs e)
        {
            PointF[] pointsF = new PointF[points.Length];
            for (int i = 0; i < points.Length; i++)
            {
                PointF pointf = axis switch
                {
                    Axis.X => new((float)points[i].Y, (float)points[i].Z),
                    Axis.Y => new((float)points[i].X, (float)points[i].Z),
                    _ => points[i].ToPointF(),
                };
                pointf = new((float)((pointf.X * ScaleFactor) + DrawOffset), (float)((pointf.Y * ScaleFactor) + DrawOffset));
                pointsF[i] = pointf;
            }

            if (isFilled)
            {
                e.Graphics.FillPolygon(Brushes.WhiteSmoke, pointsF);
            }

            e.Graphics.DrawPolygon(Pens.White, pointsF);
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            // Increase the angle
            angleX += (Math.PI / 180) + IncrementAngleX;
            angleY += (Math.PI / 180) + IncrementAngleY;
            angleZ += (Math.PI / 180) + IncrementAngleZ;

            // Create the transformation matrix
            Matrix4 transform = Matrix4.CreateRotationY(angleY) * Matrix4.CreateRotationX(angleX) * Matrix4.CreateRotationZ(angleZ);

            // Transform the cube
            transformedCube.Clear();
            foreach (Face face in cube)
            {
                transformedCube.Add(face * transform);
            }

            // Refresh the window
            Refresh();
        }

        private void ChbFill_CheckedChanged(object sender, EventArgs e) => isFilled = chbFill.Checked;

        private void ChbTimer_CheckedChanged(object sender, EventArgs e) => fpsTimer.Enabled = chbTimer.Checked;

        private void RefreshForCheck(object sender, EventArgs e)
        {
            Refresh();
        }
    }
}
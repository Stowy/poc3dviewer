﻿/**
* @file Face.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Face class.
* @version 1.0
* @date 06.04.2021
*
* @copyright CFPT (c) 2021
*
*/
namespace POC3DViewer.Mathematics
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents one face of a 3D model.
    /// </summary>
    public struct Face
    {
        /// <summary>
        /// The minimum amounts of points a face should have.
        /// </summary>
        public const int MinPoints = 3;

        private readonly Vector4[] vertices;
        private readonly Vector3 normal;

        /// <summary>
        /// Initializes a new instance of the <see cref="Face"/> struct.
        /// </summary>
        /// <param name="points">The vertices that the face should contain.</param>
        /// <exception cref="ArgumentException">Thrown when there are less than 3 points.</exception>
        public Face(Vector4[] points)
        {
            if (points.Length < MinPoints)
            {
                throw new ArgumentException($"The parameter \"{nameof(points)}\" should have a length bigger than {MinPoints}.");
            }

            vertices = points;

            Vector3 physicalTwo = vertices[2].ToPhysicalCoords();
            Vector3 v0 = vertices[0].ToPhysicalCoords() - physicalTwo;
            Vector3 v1 = vertices[1].ToPhysicalCoords() - physicalTwo;
            normal = v0.Cross(v1).NormalizeOrDefault();
        }

        /// <summary>
        /// Gets the vertices of the face.
        /// </summary>
        public Vector4[] Vertices => vertices;

        /// <summary>
        /// Gets the normal of the face.
        /// </summary>
        public Vector3 Normal => normal;

        /// <summary>
        /// Gets the center of the face.
        /// </summary>
        public Vector3 Center
        {
            get
            {
                Vector3 center = new(0, 0, 0);
                foreach (var vertex in Vertices)
                {
                    center += vertex.ToPhysicalCoords();
                }

                center /= Vertices.Length;

                return center;
            }
        }

        /// <summary>
        /// Multiplies the vertices of the face by the matrix.
        /// </summary>
        /// <param name="f1">The face to multiply.</param>
        /// <param name="m2">The matrix to multiply the face by.</param>
        /// <returns>The multiplied face.</returns>
        public static Face operator *(Face f1, Matrix4 m2)
        {
            List<Vector4> newVertices = new();

            foreach (Vector4 vertex in f1.Vertices)
            {
                newVertices.Add(vertex * m2);
            }

            return new(newVertices.ToArray());
        }

        /// <summary>
        /// Converts the vertices of this face to physical coordinates.
        /// </summary>
        /// <returns>The vertices of this face as physical coordinates.</returns>
        public Vector3[] ToPhysicalCoords()
        {
            Vector3[] physical = new Vector3[vertices.Length];
            for (int i = 0; i < physical.Length; i++)
            {
                physical[i] = vertices[i].ToPhysicalCoords();
            }

            return physical;
        }
    }
}

﻿/**
 * @file DoubleExtension.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the DoubleExtension class.
 * @version 1.0
 * @date 11.01.2021
 *
 * @copyright CFPT (c) 2020
 *
 */
namespace POC3DViewer.Mathematics
{
    using System;

    /// <summary>
    /// Class that contains all the extensions for the double class.
    /// </summary>
    public static class DoubleExtension
    {
        /// <summary>
        /// Comparator within an absolute tolerance.
        /// </summary>
        /// <param name="a">The double to compare to.</param>
        /// <param name="b">The double to compare with.</param>
        /// <param name="maxAbsoluteError">The tolerance for the comparison compared against the difference of the two doubles.</param>
        /// <returns>True if the doubles are equal within a tolerance.</returns>
        public static bool AlmostEqualsWithAbsTolerance(this double a, double b, double maxAbsoluteError)
        {
            double diff = Math.Abs(a - b);

            // Shortcut, handles infinities
            return a.Equals(b) || diff <= maxAbsoluteError;
        }
    }
}

﻿/**
 * @file Vector3.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Vector3 class.
 * @version 1.0
 * @date 04.01.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace POC3DViewer.Mathematics
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Text;

    /// <summary>
    /// Represents a 3D vector.
    /// </summary>
    /// <remarks>
    /// Based on this tutorial : https://www.codeproject.com/articles/17425/a-vector-type-for-c.
    /// </remarks>
    [ImmutableObject(true)]
    public struct Vector3
        : IComparable, IComparable<Vector3>, IEquatable<Vector3>, IFormattable
    {
        #region Constants

        /// <summary>
        /// An origin vector. Is equal to Vector(0, 0, 0). Same as Zero.
        /// </summary>
        public static readonly Vector3 Origin = new(0, 0, 0);

        /// <summary>
        /// Represents the X axis. Is equal to Vector(1, 0, 0).
        /// </summary>
        public static readonly Vector3 XAxis = new(1, 0, 0);

        /// <summary>
        /// Represents the Y axis. Is equal to Vector(0, 1, 0).
        /// </summary>
        public static readonly Vector3 YAxis = new(0, 1, 0);

        /// <summary>
        /// Represents the Z axis. Is equal to Vector(0, 0, 1).
        /// </summary>
        public static readonly Vector3 ZAxis = new(0, 0, 1);

        /// <summary>
        /// A vector with every components equal to double.MinValue.
        /// </summary>
        public static readonly Vector3 MinValue = new(double.MinValue, double.MinValue, double.MinValue);

        /// <summary>
        /// A vector with every components equal to double.MaxValue.
        /// </summary>
        public static readonly Vector3 MaxValue = new(double.MaxValue, double.MaxValue, double.MaxValue);

        /// <summary>
        /// A vector with every components equal to double.Epsilon.
        /// </summary>
        public static readonly Vector3 Epsilon = new(double.Epsilon, double.Epsilon, double.Epsilon);

        /// <summary>
        /// A zero vector. Is equal to Vector(0, 0, 0). Same as Origin.
        /// </summary>
        public static readonly Vector3 Zero = Origin;

        /// <summary>
        /// A vector with every components equal to double.NaN.
        /// </summary>
        public static readonly Vector3 NaN = new(double.NaN, double.NaN, double.NaN);

        private const string ThreeComponentsException = "Array must contain exactly three components, [x, y, z]";
        private const string NonVectorComparaison = "Cannot compare a Vector3 to a non-Vector3\nThe argument provided is a type of {0}";
        private const string NormalizeZero = "Cannot normalize a vector when it's magnitude is zero";
        private const string NormalizeNaN = "Cannot normalize a vector when it's magnitude is NaN";
        private const string NormalizeInf = "Cannot normalize a vector when it's magnitude is infinite";
        private const string NegativeMagnitude = "The magnitude of a Vector3 must be a positive value, (i.e. greater than 0)";
        private const string OriginVectorMagnitude = "Cannot change the magnitude of Vector3(0, 0, 0)";
        private const string ArgumentValue = "The argument provided has a value of ";
        private const string InterpolationRange = "Control parameter must be a value between 0 & 1";
        #endregion

        /// <summary>
        /// The X component of the vector.
        /// </summary>
        private readonly double x;

        /// <summary>
        /// The Y component of the vector.
        /// </summary>
        private readonly double y;

        /// <summary>
        /// The Z component of the vector.
        /// </summary>
        private readonly double z;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3"/> struct.
        /// </summary>
        /// <param name="x">X component of the vector.</param>
        /// <param name="y">Y component of the vector.</param>
        /// <param name="z">Z component of the vector.</param>
        public Vector3(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3"/> struct.
        /// </summary>
        /// <param name="xyz">Array with the X, Y and Z components fo the vector.</param>
        /// <exception cref="System.ArgumentException">
        /// Thrown if the array argument does not contain exactly three components.
        /// </exception>
        public Vector3(double[] xyz)
        {
            if (xyz.Length == 3)
            {
                x = xyz[0];
                y = xyz[1];
                z = xyz[2];
            }
            else
            {
                throw new ArgumentException(ThreeComponentsException);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Vector3"/> struct.
        /// </summary>
        /// <param name="v1">Vector3 representing the new values for the vector.</param>
        public Vector3(Vector3 v1)
        {
            x = v1.X;
            y = v1.Y;
            z = v1.Z;
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the X component of the vector.
        /// </summary>
        public double X => x;

        /// <summary>
        /// Gets the Y component of the vector.
        /// </summary>
        public double Y => y;

        /// <summary>
        /// Gets the Z component of the vector.
        /// </summary>
        public double Z => z;

        /// <summary>
        /// Gets the vector as an array.
        /// </summary>
        public double[] Array => new double[] { X, Y, Z };

        /// <summary>
        /// Gets the magnitude (aka. length or absolute value) of the vector.
        /// </summary>
        public double Magnitude => Math.Sqrt(SqrMagnitude);

        /// <summary>
        /// Gets the squared magnitude of this vector, can be used for better performance than Magnitude.
        /// </summary>
        public double SqrMagnitude => SqrComponents().SumComponents();

        /// <summary>
        /// Gets this vector but normalized.
        /// </summary>
        public Vector3 Normalized => Normalize(this);
        #endregion

        #region Operators

        /// <summary>
        /// An index accessor for a vector, mapping index [0] -> X, [1] -> Y and  [2] -> Z.
        /// </summary>
        /// <param name="index">The array index referring to a component within the vector (i.e. x, y, z).</param>
        /// <returns>Returns X if 0, Y if 1 and Z if 2.</returns>
        public double this[int index] => index switch
        {
            0 => X,
            1 => Y,
            2 => Z,
            _ => throw new ArgumentException(ThreeComponentsException),
        };

        public static Vector3 operator +(Vector3 v1, Vector3 v2) => new(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);

        public static Vector3 operator -(Vector3 v1, Vector3 v2) => new(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);

        public static Vector3 operator -(Vector3 v1) => new(-v1.X, -v1.Y, -v1.Z);

        public static Vector3 operator +(Vector3 v1) => new(+v1.X, +v1.Y, +v1.Z);

        public static bool operator <(Vector3 v1, Vector3 v2) => v1.SqrMagnitude < v2.SqrMagnitude;

        public static bool operator <=(Vector3 v1, Vector3 v2) => v1.SqrMagnitude <= v2.SqrMagnitude;

        public static bool operator >(Vector3 v1, Vector3 v2) => v1.SqrMagnitude > v2.SqrMagnitude;

        public static bool operator >=(Vector3 v1, Vector3 v2) => v1.SqrMagnitude >= v2.SqrMagnitude;

        public static bool operator ==(Vector3 v1, Vector3 v2) => v1.X == v2.X && v1.Y == v2.Y && v1.Z == v2.Z;

        public static bool operator !=(Vector3 v1, Vector3 v2) => !(v1 == v2);

        public static Vector3 operator *(Vector3 v1, double s2) => new(v1.X * s2, v1.Y * s2, v1.Z * s2);

        public static Vector3 operator *(double s1, Vector3 v2) => v2 * s1;

        public static Vector3 operator /(Vector3 v1, double s2) => new(v1.X / s2, v1.Y / s2, v1.Z / s2);
        #endregion

        #region Static Methods

        /// <summary>
        /// Determines the dot product of two vectors.
        /// </summary>
        /// <param name="v1">The vector to multiply.</param>
        /// <param name="v2">The vector to multiply by.</param>
        /// <returns>Returns a scalar representing the dot product of the two vectors.</returns>
        public static double Dot(Vector3 v1, Vector3 v2) => (v1.X * v2.X) + (v1.Y * v2.Y) + (v1.Z * v2.Z);

        /// <summary>
        /// Determine the cross product of two Vectors.
        /// Determine the vector product.
        /// Determine the normal vector (Vector3 90° to the plane).
        /// </summary>
        /// <param name="v1">The vector to multiply.</param>
        /// <param name="v2">The vector to multiply by.</param>
        /// <returns>Vector3 representig the cross product of the two vectors.</returns>
        public static Vector3 Cross(Vector3 v1, Vector3 v2) => new((v1.Y * v2.Z) - (v1.Z * v2.Y), (v1.Z * v2.X) - (v1.X * v2.Z), (v1.X * v2.Z) - (v1.Y * v2.X));

        /// <summary>
        /// Checks if the vector is a unit vector.
        /// Checks if the vector has be normalized.
        /// Checks if the vector has a magnitude of 1.
        /// </summary>
        /// <param name="v1">The vector to be checked for normalization.</param>
        /// <returns>Returns true if the vector is a unit vector.</returns>
        public static bool IsUnitVector(Vector3 v1) => v1.Magnitude == 1;

        /// <summary>
        /// Checks if the vector is a unit vector within a tolerance.
        /// Checks if the vector has been normalized within a tolerance.
        /// Checks if the vector has a magnitude of 1 within a tolerance.
        /// </summary>
        /// <param name="v1">The vector to be checked for normalization.</param>
        /// <param name="tolerance">The tolerance to use when comparing the magnitude.</param>
        /// <returns>Returns true if the vector is a unit vector.</returns>
        public static bool IsUnitVector(Vector3 v1, double tolerance) => v1.Magnitude.AlmostEqualsWithAbsTolerance(1, tolerance);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <param name="v1">The vector to be normalized.</param>
        /// <returns>Returns the normalized vector.</returns>
        /// <exception cref="NormalizeVectorException">
        /// Thrown when the normalisation of a zero magnitude vector is attempted.
        /// </exception>
        /// <exception cref="NormalizeVectorException">
        /// Thrown when the normalisation of a NaN magnitude vector is attempted.
        /// </exception>
        /// <remarks>
        /// Exceptions will be thrown if the vector being normalized has a magnitude of 0 or of NaN.
        /// </remarks>
        public static Vector3 Normalize(Vector3 v1)
        {
            double magnitude = v1.Magnitude;

            if (double.IsInfinity(magnitude))
            {
                v1 = NormalizeSpecialCasesOrOrigional(v1);

                if (v1.IsNaN())
                {
                    // If this wasn't a special case, throw an exception
                    throw new NormalizeVectorException(NormalizeInf);
                }
            }

            // Check that we are not trying to normalize a vector of magnitude 0
            if (magnitude == 0)
            {
                throw new NormalizeVectorException(NormalizeZero);
            }

            // Check that we are not trying to normalize a vector of magnitude NaN
            if (double.IsNaN(magnitude))
            {
                throw new NormalizeVectorException(NormalizeNaN);
            }

            return NormalizeOrNaN(v1);
        }

        /// <summary>
        /// Checks if any component of a vector is Not A Number (NaN).
        /// </summary>
        /// <param name="v1">The vector checked for NaN components.</param>
        /// <returns>Returns true if the vector has NaN components.</returns>
        public static bool IsNaN(Vector3 v1) => double.IsNaN(v1.X) || double.IsNaN(v1.Y) || double.IsNaN(v1.Z);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <param name="v1">The vector to be normalized.</param>
        /// <returns>Returns Vector (0,0,0) if the magnitude is zero, Vector (NaN, NaN, NaN) if magnitude is NaN, or normalized vector.</returns>
        public static Vector3 NormalizeOrDefault(Vector3 v1)
        {
            // Special cases
            v1 = NormalizeSpecialCasesOrOrigional(v1);

            // Check that we are not trying to normalize with a vector of magnitude 0. If yes, we return v(0, 0, 0).
            if (v1.Magnitude == 0)
            {
                return Origin;
            }

            // Check that we are not trying to normalize a vector with a NaN component. If yes, we return v(NaN, NaN, NaN).
            if (v1.IsNaN())
            {
                return NaN;
            }

            return NormalizeOrNaN(v1);
        }

        /// <summary>
        /// Computes the angle between two vectors.
        /// </summary>
        /// <param name="v1">The vector to discern the angle from.</param>
        /// <param name="v2">The vector to discern the angle to.</param>
        /// <returns>Returns the angle between the two vectors.</returns>
        public static double Angle(Vector3 v1, Vector3 v2)
        {
            if (v1 == v2)
            {
                return 0;
            }

            return Math.Acos(Math.Min(1.0f, NormalizeOrDefault(v1).Dot(NormalizeOrDefault(v2))));
        }

        /// <summary>
        /// Finds the absolute value of a vector.
        /// Finds the magnitude of a vector.
        /// </summary>
        /// <param name="v1">The vector to get the magnitude from.</param>
        /// <returns>Returns a double representig the magnitude of the vector.</returns>
        public static double Abs(Vector3 v1) => v1.Magnitude;

        /// <summary>
        /// Computes the distance between two vectors.
        /// </summary>
        /// <param name="v1">The vector to find the distance from.</param>
        /// <param name="v2">The vector to find the distance to.</param>
        /// <returns>Returns the distance between two vectors.</returns>
        public static double Distance(Vector3 v1, Vector3 v2) => Math.Sqrt(
            ((v1.X - v2.X) * (v1.X - v2.X)) +
            ((v1.Y - v2.Y) * (v1.Y - v2.Y)) +
            ((v1.Z - v2.Z) * (v1.Z - v2.Z)));

        /// <summary>
        /// Compares the magnitude of two vectors and return the greater.
        /// </summary>
        /// <param name="v1">The vector to compare.</param>
        /// <param name="v2">The vector to compare with.</param>
        /// <returns>The greater of the two Vectors (based on magnitude).</returns>
        public static Vector3 Max(Vector3 v1, Vector3 v2) => v1 >= v2 ? v1 : v2;

        /// <summary>
        /// Compares the magnitude of two vectors and returns the lesser.
        /// </summary>
        /// <param name="v1">The vector to compare.</param>
        /// <param name="v2">The vector to compare with.</param>
        /// <returns>Returns the lesser of the two vectors.</returns>
        public static Vector3 Min(Vector3 v1, Vector3 v2) => v1 <= v2 ? v1 : v2;

        /// <summary>
        /// Rounds the vector to the nearest integral value.
        /// </summary>
        /// <param name="v1">The vector to round.</param>
        /// <returns>The rounded vector.</returns>
        public static Vector3 Round(Vector3 v1) => new(Math.Round(v1.X), Math.Round(v1.Y), Math.Round(v1.Z));

        /// <summary>
        /// Rounds the vector to the nearest integral value.
        /// </summary>
        /// <param name="v1">The vector to round.</param>
        /// <param name="mode">An enum to specify how rounding should happen for numbers midway between two other number.</param>
        /// <returns>The rounded vector.</returns>
        public static Vector3 Round(Vector3 v1, MidpointRounding mode) => new(Math.Round(v1.X, mode), Math.Round(v1.Y, mode), Math.Round(v1.Z, mode));

        /// <summary>
        /// Scales the vector.
        /// Changes the magnitude of the vector.
        /// </summary>
        /// <param name="vector">Vector to scale.</param>
        /// <param name="magnitude">The magnitude to be set.</param>
        /// <returns>The scaled vector.</returns>
        public static Vector3 Scale(Vector3 vector, double magnitude)
        {
            if (magnitude < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(magnitude), magnitude, NegativeMagnitude);
            }

            if (vector == Zero)
            {
                throw new ArgumentException(OriginVectorMagnitude, nameof(vector));
            }

            return vector * (magnitude / vector.Magnitude);
        }

        /// <summary>
        /// Sums the components of the vector.
        /// </summary>
        /// <param name="v1">The vector whose scalar components to sum.</param>
        /// <returns>The sums of the vector's X, Y and Z components.</returns>
        public static double SumComponents(Vector3 v1) => v1.X + v1.Y + v1.Z;

        /// <summary>
        /// The individual multiplication to a power of the vectors's components.
        /// </summary>
        /// <param name="v1">The vector whose scalar components to multiply by a power.</param>
        /// <param name="power">The power by which to multiply the components.</param>
        /// <returns>The multiplied vector.</returns>
        public static Vector3 PowComponents(Vector3 v1, double power) => new(Math.Pow(v1.X, power), Math.Pow(v1.Y, power), Math.Pow(v1.Z, power));

        /// <summary>
        /// The individual square root of a vectors's components.
        /// </summary>
        /// <param name="v1">The vector whose scalar components to square root.</param>
        /// <returns>The rooted vector.</returns>
        public static Vector3 SqrtComponents(Vector3 v1) => new(Math.Sqrt(v1.X), Math.Sqrt(v1.Y), Math.Sqrt(v1.Z));

        /// <summary>
        /// The vectors's components squared.
        /// </summary>
        /// <param name="v1">The vector whose scalar components are to square.</param>
        /// <returns>The squared vectors.</returns>
        public static Vector3 SqrComponents(Vector3 v1) => new(v1.X * v1.X, v1.Y * v1.Y, v1.Z * v1.Z);

        /// <summary>
        /// Takes an interpolated value from between two vectors or an extrapolated value if allowed.
        /// </summary>
        /// <param name="v1">The Vector3 to interpolate from (where control == 0).</param>
        /// <param name="v2">The Vector3 to interpolate to (where control == 1).</param>
        /// <param name="control">The interpolated point between the two vectors to retrieve (fraction between 0 and 1), or an extrapolated point if allowed.</param>
        /// <param name="allowExtrapolation">True if the control may represent a point not on the vertex between v1 and v2.</param>
        /// <returns>The value at an arbitrary distance (interpolation) between two vectors or an extrapolated point on the extended vertex.</returns>
        public static Vector3 Interpolate(Vector3 v1, Vector3 v2, double control, bool allowExtrapolation = false)
        {
            if (!allowExtrapolation && (control > 1 || control < 0))
            {
                throw new ArgumentOutOfRangeException(nameof(control), control, $"{InterpolationRange}\n{ArgumentValue}{control}");
            }

            return new(
                (v1.X * (1 - control)) + (v2.X * control),
                (v1.Y * (1 - control)) + (v2.Y * control),
                (v1.Z * (1 - control)) + (v2.Z * control));
        }

        /// <summary>
        /// Checks if a face normal vector represents back face.
        /// Checks if a face is visible, given the line of sight.
        /// </summary>
        /// <param name="normal">The vector representing the face normal Vector3.</param>
        /// <param name="lineOfSight">The unit vector representing the direction of sight from a virtual camera.</param>
        /// <returns>True if the vector (as a normal) represents a back-face.</returns>
        public static bool IsBackFace(Vector3 normal, Vector3 lineOfSight) => normal.Dot(lineOfSight) < 0;

        /// <summary>
        /// Determine the mixed product of three vectors.
        /// Determine volume (with sign precision) of parallelepiped spanned on given vectors.
        /// Determine the scalar triple product of three vectors.
        /// </summary>
        /// <param name="v1">The first vector.</param>
        /// <param name="v2">The second vector.</param>
        /// <param name="v3">The third vector.</param>
        /// <returns>Scalar representing the mixed product of the three vectors.</returns>
        public static double MixedProduct(Vector3 v1, Vector3 v2, Vector3 v3) => Dot(Cross(v1, v2), v3);

        /// <summary>
        /// Checks if two vectors are perpendicular.
        /// Checks if two vectors are orthogonal.
        /// Checks if one vector is the normal of the other.
        /// </summary>
        /// <param name="v1">The vector to be checked for orthogonality.</param>
        /// <param name="v2">The vector to be checked for orthogonality to.</param>
        /// <returns>True if the two Vectors are perpendicular.</returns>
        public static bool IsPerpendicular(Vector3 v1, Vector3 v2)
        {
            // Use normalization of special cases to handle special cases of IsPerpendicular
            v1 = NormalizeSpecialCasesOrOrigional(v1);
            v2 = NormalizeSpecialCasesOrOrigional(v2);

            // If either vector is v(0, 0, 0) the vectors ar not perpendicular
            if (v1 == Zero || v2 == Zero)
            {
                return false;
            }

            // Is perpendicular
            return v1.Dot(v2).Equals(0);
        }

        /// <summary>
        /// Checks if two vectors are perpendicular within a tolerance.
        /// Checks if two vectors are orthogonal within a tolerance.
        /// Checks if one vector is the normal of the other within a tolerance.
        /// </summary>
        /// <param name="v1">The vector to be checked for orthogonality.</param>
        /// <param name="v2">The vector to be checked for orthogonality to.</param>
        /// <param name="tolerance">The absolute difference tolerance to use when comparing the dot product to 0.</param>
        /// <returns>True if the two vectors are perpendicular within a tolerance.</returns>
        public static bool IsPerpendicular(Vector3 v1, Vector3 v2, double tolerance)
        {
            // Use normalization of special cases to handle special cases of IsPerpendicular
            v1 = NormalizeSpecialCasesOrOrigional(v1);
            v2 = NormalizeSpecialCasesOrOrigional(v2);

            // If either vector is v(0, 0, 0) the vectors ar not perpendicular
            if (v1 == Zero || v2 == Zero)
            {
                return false;
            }

            // Is perpendicular
            return v1.Dot(v2).AlmostEqualsWithAbsTolerance(0, tolerance);
        }

        /// <summary>
        /// Projects the specified v1 onto the specified v2.
        /// The vector resolute of v1 in the direction of v2.
        /// </summary>
        /// <param name="v1">The vector that will be projected.</param>
        /// <param name="v2">A nonzero vector that v1 will be projected upon. The direction to project.</param>
        /// <returns>The projected vector.</returns>
        public static Vector3 Projection(Vector3 v1, Vector3 v2) => new(v2 * (v1.Dot(v2) / Math.Pow(v2.Magnitude, 2)));

        /// <summary>
        /// Vector rejection of the specified v1 onto the specified v2.
        /// </summary>
        /// <param name="v1">The vector subject of vector rejection.</param>
        /// <param name="v2">>A nonzero vector for which the vector rejection of v1 will be calculated. The direction.</param>
        /// <returns>The vector rejection of v1 onto v2.</returns>
        public static Vector3 Rejection(Vector3 v1, Vector3 v2) => v1 - v1.Projection(v2);

        /// <summary>
        /// Reflect v1 about v2.
        /// </summary>
        /// <param name="v1">The vector to be reflected.</param>
        /// <param name="v2">The vector to be reflect about.</param>
        /// <returns>The reflected vector.</returns>
        public static Vector3 Reflection(Vector3 v1, Vector3 v2)
        {
            // If v2 has a right angle to v1, return -v1 and stop
            if (Math.Abs(Math.Abs(v1.Angle(v2)) - (Math.PI / 2)) < double.Epsilon)
            {
                return -v1;
            }

            return new Vector3((2 * v1.Projection(v2)) - v1).Scale(v1.Magnitude);
        }

        /// <summary>
        /// Rotates a vector around the X axis.
        /// </summary>
        /// <param name="v1">The vector to be rotated.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the X axis.</returns>
        public static Vector3 RotateX(Vector3 v1, double rad)
        {
            double x = v1.X;
            double y = (v1.Y * Math.Cos(rad)) - (v1.Z * Math.Sin(rad));
            double z = (v1.Y * Math.Sin(rad)) + (v1.Z * Math.Cos(rad));

            return new(x, y, z);
        }

        /// <summary>
        /// Rotates a vector around the Y axis.
        /// </summary>
        /// <param name="v1">The vector to be rotated.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the Y axis.</returns>
        public static Vector3 RotateY(Vector3 v1, double rad)
        {
            double x = (v1.Z * Math.Sin(rad)) + (v1.X * Math.Cos(rad));
            double y = v1.Y;
            double z = (v1.Z * Math.Cos(rad)) - (v1.X * Math.Sin(rad));

            return new(x, y, z);
        }

        /// <summary>
        /// Rotates a vector around the Z axis.
        /// </summary>
        /// <param name="v1">The vector to be rotated.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the Z axis.</returns>
        public static Vector3 RotateZ(Vector3 v1, double rad)
        {
            double x = (v1.X * Math.Cos(rad)) - (v1.Y * Math.Sin(rad));
            double y = (v1.X * Math.Sin(rad)) + (v1.Y * Math.Cos(rad));
            double z = v1.Z;

            return new(x, y, z);
        }

        /// <summary>
        /// Rotates a vector around the X axis with offsets for Y and Z.
        /// </summary>
        /// <param name="v1">The vector to be rotated.</param>
        /// <param name="yOff">The Y axis offset.</param>
        /// <param name="zOff">The Z axis offset.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>Vector representing the rotation around the X axis.</returns>
        public static Vector3 RotateX(Vector3 v1, double yOff, double zOff, double rad)
        {
            double x = v1.X;
            double y = (v1.Y * Math.Cos(rad)) - (v1.Z * Math.Sin(rad)) + ((yOff * (1 - Math.Cos(rad))) + (zOff * Math.Sin(rad)));
            double z = (v1.Y * Math.Sin(rad)) + (v1.Z * Math.Cos(rad)) + ((zOff * (1 - Math.Cos(rad))) - (yOff * Math.Sin(rad)));

            return new(x, y, z);
        }

        /// <summary>
        /// Rotates a vector around the Y axis with offsets for X and Z.
        /// </summary>
        /// <param name="v1">The vector to be rotated.</param>
        /// <param name="xOff">The X axis offset.</param>
        /// <param name="zOff">The Z axis offset.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>Vector representing the rotation around the Y axis.</returns>
        public static Vector3 RotateY(Vector3 v1, double xOff, double zOff, double rad)
        {
            double x = (v1.Z * Math.Sin(rad)) + (v1.X * Math.Cos(rad)) + ((xOff * (1 - Math.Cos(rad))) - (zOff * Math.Sin(rad)));
            double y = v1.Y;
            double z = (v1.Z * Math.Cos(rad)) - (v1.X * Math.Sin(rad)) + ((zOff * (1 - Math.Cos(rad))) + (xOff * Math.Sin(rad)));

            return new(x, y, z);
        }

        /// <summary>
        /// Rotates a vector around the Z axis with offsets for X and Y.
        /// </summary>
        /// <param name="v1">The vector to be rotated.</param>
        /// <param name="xOff">The X axis offset.</param>
        /// <param name="yOff">The Y axis offset.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>Vector representing the rotation around the Z axis.</returns>
        public static Vector3 RotateZ(Vector3 v1, double xOff, double yOff, double rad)
        {
            double x = (v1.X * Math.Cos(rad)) - (v1.Y * Math.Sin(rad)) + ((xOff * (1 - Math.Cos(rad))) + (yOff * Math.Sin(rad)));
            double y = (v1.X * Math.Sin(rad)) + (v1.Y * Math.Cos(rad)) + ((yOff * (1 - Math.Cos(rad))) - (xOff * Math.Sin(rad)));
            double z = v1.Z;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Rotates a vector around the X axis.
        /// Change the pitch of a vector.
        /// </summary>
        /// <param name="v1">The vector to be rotated.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the X axis.</returns>
        public static Vector3 Pitch(Vector3 v1, double rad) => RotateX(v1, rad);

        /// <summary>
        /// Rotates a vector around the Y axis.
        /// Change the yaw of a vector.
        /// </summary>
        /// <param name="v1">The vector to be rotated.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the Y axis.</returns>
        public static Vector3 Yaw(Vector3 v1, double rad) => RotateY(v1, rad);

        /// <summary>
        /// Rotates a vector around the Z axis.
        /// Change the roll of a vector.
        /// </summary>
        /// <param name="v1">The Vector3 to be rotated.</param>
        /// <param name="rad">The angle to rotate the Vector3 around in radians.</param>
        /// <returns>Vector3 representing the rotation around the Z axis.</returns>
        public static Vector3 Roll(Vector3 v1, double rad) => RotateZ(v1, rad);
        #endregion

        #region Methods

        /// <summary>
        /// Determines the dot product of two vectors.
        /// </summary>
        /// <param name="other">The vector to multiply by.</param>
        /// <returns>Returns a scalar representing the dot product of the two vectors.</returns>
        public double Dot(Vector3 other) => Dot(this, other);

        /// <summary>
        /// Determine the cross product of two Vectors.
        /// Determine the vector product.
        /// Determine the normal vector (Vector3 90° to the plane).
        /// </summary>
        /// <param name="other">The vector to multiply by.</param>
        /// <returns>Vector3 representig the cross product of the two vectors.</returns>
        public Vector3 Cross(Vector3 other) => Cross(this, other);

        /// <summary>
        /// Checks if the vector is a unit vector.
        /// Checks if the vector has be normalized.
        /// Checks if the vector has a magnitude of 1.
        /// </summary>
        /// <returns>Returns true if the vector is a unit vector.</returns>
        public bool IsUnitVector() => IsUnitVector(this);

        /// <summary>
        /// Checks if the vector is a unit vector within a tolerance.
        /// Checks if the vector has been normalized within a tolerance.
        /// Checks if the vector has a magnitude of 1 within a tolerance.
        /// </summary>
        /// <param name="tolerance">The tolerance to use when comparing the magnitude.</param>
        /// <returns>Returns true if the vector is a unit vector.</returns>
        public bool IsUnitVector(double tolerance) => IsUnitVector(this, tolerance);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <returns>Returns the normalized vector.</returns>
        /// <exception cref="NormalizeVectorException">
        /// Thrown when the normalisation of a zero magnitude vector is attempted.
        /// </exception>
        /// <exception cref="NormalizeVectorException">
        /// Thrown when the normalisation of a NaN magnitude vector is attempted.
        /// </exception>
        /// <remarks>
        /// Exceptions will be thrown if the vector being normalized has a magnitude of 0 or of NaN.
        /// </remarks>
        public Vector3 Normalize() => Normalize(this);

        /// <summary>
        /// Checks if any component of a vector is Not A Number (NaN).
        /// </summary>
        /// <returns>Returns true if the vector has NaN components.</returns>
        public bool IsNaN() => IsNaN(this);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <returns>Returns Vector (0, 0, 0) if the magnitude is zero, Vector (NaN, NaN, NaN) if magnitude is NaN, or normalized vector.</returns>
        public Vector3 NormalizeOrDefault() => NormalizeOrDefault(this);

        /// <summary>
        /// Finds the absolute value of a vector.
        /// Finds the magnitude of a vector.
        /// </summary>
        /// <returns>Returns a double representig the magnitude of the vector.</returns>
        public double Abs() => Magnitude;

        /// <summary>
        /// Computes the angle between two vectors.
        /// </summary>
        /// <param name="other">The vector to discern the angle to.</param>
        /// <returns>Returns the angle between the two vectors.</returns>
        public double Angle(Vector3 other) => Angle(this, other);

        /// <summary>
        /// Computes the distance between two vectors.
        /// </summary>
        /// <param name="other">The vector to find the distance to.</param>
        /// <returns>Returns the distance between two vectors.</returns>
        public double Distance(Vector3 other) => Distance(this, other);

        /// <summary>
        /// Compares the magnitude of two vectors and return the greater.
        /// </summary>
        /// <param name="other">The vector to compare with.</param>
        /// <returns>Returns the greater of the two Vectors (based on magnitude).</returns>
        public Vector3 Max(Vector3 other) => Max(this, other);

        /// <summary>
        /// Compares the magnitude of two vectors and returns the lesser.
        /// </summary>
        /// <param name="other">The vector to compare with.</param>
        /// <returns>Returns the lesser of the two vectors.</returns>
        public Vector3 Min(Vector3 other) => Min(this, other);

        /// <summary>
        /// Rounds the vector to the nearest integral value.
        /// </summary>
        /// <returns>The rounded vector.</returns>
        public Vector3 Round() => Round(this);

        /// <summary>
        /// Rounds the vector to the nearest integral value.
        /// </summary>
        /// <param name="mode">An enum to specify how rounding should happen for numbers midway between two other number.</param>
        /// <returns>The rounded vector.</returns>
        public Vector3 Round(MidpointRounding mode) => Round(this, mode);

        /// <summary>
        /// Scales the vector.
        /// Changes the magnitude of the vector.
        /// </summary>
        /// <param name="magnitude">The magnitude to be set.</param>
        /// <returns>The scaled vector.</returns>
        public Vector3 Scale(double magnitude) => Scale(this, magnitude);

        /// <summary>
        /// Sums the components of the vector.
        /// </summary>
        /// <returns>The sums of the vector's X, Y and Z components.</returns>
        public double SumComponents() => SumComponents(this);

        /// <summary>
        /// The individual multiplication to a power of the vectors's components.
        /// </summary>
        /// <param name="power">The power by which to multiply the components.</param>
        /// <returns>The multiplied vector.</returns>
        public Vector3 PowComponents(double power) => PowComponents(this, power);

        /// <summary>
        /// The individual square root of a vectors's components.
        /// </summary>
        /// <returns>The rooted vector.</returns>
        public Vector3 SqrtComponents() => SqrtComponents(this);

        /// <summary>
        /// The vectors's components squared.
        /// </summary>
        /// <returns>The squared vectors.</returns>
        public Vector3 SqrComponents() => SqrComponents(this);

        /// <summary>
        /// Verbose textual description of the vector.
        /// </summary>
        /// <returns>Text (string) representing the vector.</returns>
        public string ToVerbString()
        {
            StringBuilder output = new();
            if (IsUnitVector())
            {
                output.Append("Unit vector composing of ");
            }
            else
            {
                output.Append("Positional vector composing of ");
            }

            output.Append($"(x={X}, y={Y}, z={Z}) of magnitude {Magnitude}");

            return output.ToString();
        }

        /// <summary>
        /// Textual description of the vector.
        /// </summary>
        /// <param name="format">Formatting string: 'x','y','z' or '' followed by standard numeric format string characters valid for a double precision floating point.</param>
        /// <param name="formatProvider">The culture specific fromatting provider.</param>
        /// <returns>Text (String) representing the vector.</returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            // If no format is passed
            if (format == null || format == string.Empty)
            {
                return $"({X}, {Y}, {Z})";
            }

            char firstChar = format[0];
            string remainder = null;

            if (format.Length > 1)
            {
                remainder = format[1..];
            }

            return firstChar switch
            {
                'x' => X.ToString(remainder, formatProvider),
                'y' => Y.ToString(remainder, formatProvider),
                'z' => Z.ToString(remainder, formatProvider),
                _ => $"({X.ToString(format, formatProvider)}, {Y.ToString(format, formatProvider)})",
            };
        }

        /// <inheritdoc/>
        public override string ToString() => ToString(null, null);

        /// <inheritdoc/>
        public override bool Equals(object other)
        {
            // Check if the other object is a Vector3
            if (other is Vector3 vector)
            {
                return vector.Equals(this);
            }

            return false;
        }

        /// <inheritdoc/>
        public bool Equals(Vector3 other) => X.Equals(other.X) && Y.Equals(other.Y) && Z.Equals(other.Z);

        /// <summary>
        /// Comparator within a tolerance.
        /// </summary>
        /// <param name="other">The other object to compare to.</param>
        /// <param name="tolerance">The tolerance to use when comparing the vector components.</param>
        /// <returns>True if two objects are Vector3s and are equal within a tolerance.</returns>
        public bool Equals(object other, double tolerance)
        {
            if (other is Vector3 vector)
            {
                return Equals(vector, tolerance);
            }

            return false;
        }

        /// <summary>
        /// Comparator within a tolerance.
        /// </summary>
        /// <param name="other">The other vector to compare to.</param>
        /// <param name="tolerance">The tolerance to use when comparing the vector components.</param>
        /// <returns>True if two vectors are equal within a tolerance.</returns>
        public bool Equals(Vector3 other, double tolerance) => X.AlmostEqualsWithAbsTolerance(other.X, tolerance) &&
            Y.AlmostEqualsWithAbsTolerance(other.Y, tolerance) &&
            Z.AlmostEqualsWithAbsTolerance(other.Z, tolerance);

        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(X, Y, Z);

        /// <inheritdoc/>
        public int CompareTo(object other)
        {
            if (other is Vector3 vector)
            {
                return CompareTo(vector);
            }

            throw new ArgumentException(string.Format(NonVectorComparaison, other.GetType()), nameof(other));
        }

        /// <inheritdoc/>
        public int CompareTo(Vector3 other)
        {
            if (this < other)
            {
                return -1;
            }
            else if (this > other)
            {
                return 1;
            }

            return 0;
        }

        /// <summary>
        /// Compares the magnitude of this instance against the magnitude of the supplied vector.
        /// </summary>
        /// <param name="other">The vector to compare this instance with.</param>
        /// <param name="tolerance">Tolerence to use when comparing the two vectors.</param>
        /// <returns>
        /// -1: The magnitude of this instance is less than the others magnitude.
        /// 0: The magnitude of this instance equals the magnitude of the other.
        /// 1: The magnitude of this instance is greater than the magnitude of the other.
        /// </returns>
        /// <remarks>
        /// Comparing two vectors has no meaning, we are comparing the magnitude of two vectors for convinience.
        /// It would be more accurate to compare the magnitudes explicitly using v1.Magnitude.CompareTo(v2.Magnitude).
        /// </remarks>
        public int CompareTo(object other, double tolerance)
        {
            if (other is Vector3 vector)
            {
                return CompareTo(vector, tolerance);
            }

            throw new ArgumentException(string.Format(NonVectorComparaison, other.GetType()), nameof(other));
        }

        /// <summary>
        /// Compares the magnitude of this instance against the magnitude of the supplied vector.
        /// </summary>
        /// <param name="other">The vector to compare this instance with.</param>
        /// <param name="tolerance">Tolerence to use when comparing the two vectors.</param>
        /// <returns>
        /// -1: The magnitude of this instance is less than the others magnitude.
        /// 0: The magnitude of this instance equals the magnitude of the other.
        /// 1: The magnitude of this instance is greater than the magnitude of the other.
        /// </returns>
        /// <remarks>
        /// Comparing two vectors has no meaning, we are comparing the magnitude of two vectors for convinience.
        /// It would be more accurate to compare the magnitudes explicitly using v1.Magnitude.CompareTo(v2.Magnitude).
        /// </remarks>
        public int CompareTo(Vector3 other, double tolerance)
        {
            bool bothInfinite = double.IsInfinity(SqrMagnitude) && double.IsInfinity(other.SqrMagnitude);

            if (Equals(other, tolerance) || bothInfinite)
            {
                return 0;
            }

            if (this < other)
            {
                return -1;
            }

            return 1;
        }

        /// <summary>
        /// Takes an interpolated value from between two vectors or an extrapolated value if allowed.
        /// </summary>
        /// <param name="other">The Vector3 to interpolate to (where control == 1).</param>
        /// <param name="control">The interpolated point between the two vectors to retrieve (fraction between 0 and 1), or an extrapolated point if allowed.</param>
        /// <param name="allowExtrapolation">True if the control may represent a point not on the vertex between v1 and v2.</param>
        /// <returns>The value at an arbitrary distance (interpolation) between two vectors or an extrapolated point on the extended vertex.</returns>
        public Vector3 Interpolate(Vector3 other, double control, bool allowExtrapolation = false) => Interpolate(this, other, control, allowExtrapolation);

        /// <summary>
        /// Checks if a face normal vector represents back face.
        /// Checks if a face is visible, given the line of sight.
        /// </summary>
        /// <param name="lineOfSight">The unit vector representing the direction of sight from a virtual camera.</param>
        /// <returns>True if the vector (as a normal) represents a back-face.</returns>
        public bool IsBackFace(Vector3 lineOfSight) => IsBackFace(this, lineOfSight);

        /// <summary>
        /// Determine the mixed product of three vectors.
        /// Determine volume (with sign precision) of parallelepiped spanned on given vectors.
        /// Determine the scalar triple product of three vectors.
        /// </summary>
        /// <param name="otherV1">The second vector.</param>
        /// <param name="otherV2">The third vector.</param>
        /// <returns>Scalar representing the mixed product of the three vectors.</returns>
        public double MixedProduct(Vector3 otherV1, Vector3 otherV2) => MixedProduct(this, otherV1, otherV2);

        /// <summary>
        /// Checks if two vectors are perpendicular.
        /// Checks if two vectors are orthogonal.
        /// Checks if one vector is the normal of the other.
        /// </summary>
        /// <param name="other">The vector to be checked for orthogonality to.</param>
        /// <returns>True if the two Vectors are perpendicular.</returns>
        public bool IsPerpendicular(Vector3 other) => IsPerpendicular(this, other);

        /// <summary>
        /// Checks if two vectors are perpendicular within a tolerance.
        /// Checks if two vectors are orthogonal within a tolerance.
        /// Checks if one vector is the normal of the other within a tolerance.
        /// </summary>
        /// <param name="other">The vector to be checked for orthogonality to.</param>
        /// <param name="tolerance">The absolute difference tolerance to use when comparing the dot product to 0.</param>
        /// <returns>True if the two vectors are perpendicular within a tolerance.</returns>
        public bool IsPerpendicular(Vector3 other, double tolerance) => IsPerpendicular(this, other, tolerance);

        /// <summary>
        /// Projects the vector onto the specified vector.
        /// The vector resolute this vector in a direction specified by a vector.
        /// </summary>
        /// <param name="direction">A nonzero vector that the vector will be projected upon. The direction to project.</param>
        /// <returns>The projected vector.</returns>
        public Vector3 Projection(Vector3 direction) => Projection(this, direction);

        /// <summary>
        /// Vector rejection of the this vector onto the specified vector.
        /// </summary>
        /// <param name="direction">A nonzero vector for which the vector rejection of this vector will be calculated. The direction.</param>
        /// <returns>The vector rejection of this vector and the given direction.</returns>
        public Vector3 Rejection(Vector3 direction) => Rejection(this, direction);

        /// <summary>
        /// Reflect this vector about the reflector.
        /// </summary>
        /// <param name="reflector">The vector to reflect about.</param>
        /// <returns>The reflected vector.</returns>
        public Vector3 Reflection(Vector3 reflector) => Reflection(this, reflector);

        /// <summary>
        /// Rotates a vector around the X axis.
        /// </summary>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the X axis.</returns>
        public Vector3 RotateX(double rad) => RotateX(this, rad);

        /// <summary>
        /// Rotates a vector around the Y axis.
        /// </summary>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the Y axis.</returns>
        public Vector3 RotateY(double rad) => RotateY(this, rad);

        /// <summary>
        /// Rotates a vector around the YZaxis.
        /// </summary>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the Z axis.</returns>
        public Vector3 RotateZ(double rad) => RotateZ(this, rad);

        /// <summary>
        /// Rotates a vector around the X axis with offsets for Y and Z.
        /// </summary>
        /// <param name="yOff">The Y axis offset.</param>
        /// <param name="zOff">The Z axis offset.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>Vector representing the rotation around the X axis.</returns>
        public Vector3 RotateX(double yOff, double zOff, double rad) => RotateX(this, yOff, zOff, rad);

        /// <summary>
        /// Rotates a vector around the Y axis with offsets for X and Z.
        /// </summary>
        /// <param name="xOff">The X axis offset.</param>
        /// <param name="zOff">The Z axis offset.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>Vector representing the rotation around the Y axis.</returns>
        public Vector3 RotateY(double xOff, double zOff, double rad) => RotateY(this, xOff, zOff, rad);

        /// <summary>
        /// Rotates a vector around the Z axis with offsets for X and Y.
        /// </summary>
        /// <param name="xOff">The X axis offset.</param>
        /// <param name="yOff">The Y axis offset.</param>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>Vector representing the rotation around the Z axis.</returns>
        public Vector3 RotateZ(double xOff, double yOff, double rad) => RotateZ(this, xOff, yOff, rad);

        /// <summary>
        /// Rotates a vector around the X axis.
        /// Change the pitch of a vector.
        /// </summary>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the X axis.</returns>
        public Vector3 Pitch(double rad) => Pitch(this, rad);

        /// <summary>
        /// Rotates a vector around the Y axis.
        /// Change the yaw of a vector.
        /// </summary>
        /// <param name="rad">The angle to rotate the vector around in radians.</param>
        /// <returns>The vector representing the rotation around the Y axis.</returns>
        public Vector3 Yaw(double rad) => Yaw(this, rad);

        /// <summary>
        /// Rotates a vector around the Z axis.
        /// Change the roll of a vector.
        /// </summary>
        /// <param name="rad">The angle to rotate the Vector3 around in radians.</param>
        /// <returns>Vector3 representing the rotation around the Z axis.</returns>
        public Vector3 Roll(double rad) => Roll(this, rad);

        /// <summary>
        /// Converts this vector to a <see cref="PointF"/>.
        /// Only keeps the X and Y components of this vector.
        /// </summary>
        /// <returns>A point with the X and Y components of this vector.</returns>
        public PointF ToPointF() => new((float)X, (float)Y);

        /// <summary>
        /// Converts this vector to a <see cref="Vector4"/>.
        /// The <see cref="Vector4.W"/> component will be equal to 1.
        /// </summary>
        /// <returns>The Vector3 as a <see cref="Vector4"/>.</returns>
        public Vector4 ToVector4() => new(x, y, z, 1);

        /// <summary>
        /// Gets the normalized unit vector with a magnitude of one.
        /// </summary>
        /// <param name="v1">The vector to be normalized.</param>
        /// <returns>The normalized vector3 or vector (NaN,NaN,NaN) if the magnitude is 0 or NaN.</returns>
        private static Vector3 NormalizeOrNaN(Vector3 v1)
        {
            // Find the inverse of the vectors magnitude
            double inverse = 1 / v1.Magnitude;

            // Multiply each component by the inverse of the magnitude
            return new Vector3(v1.X * inverse, v1.Y * inverse, v1.Z * inverse);
        }

        /// <summary>
        /// This method is used to normalize special cases of vectors where the components are infinite and/or zero only.
        /// Other vectors will be returned un-normalized.
        /// </summary>
        /// <param name="v1">The vector to be normalized if it is a special case.</param>
        /// <returns>Normialized special case vectors, NaN or the origional vector.</returns>
        private static Vector3 NormalizeSpecialCasesOrOrigional(Vector3 v1)
        {
            if (double.IsInfinity(v1.Magnitude))
            {
                double x = v1.X == 0 ? 0 :
                    v1.X == -0 ? -0 :
                    double.IsPositiveInfinity(v1.X) ? 1 :
                    double.IsNegativeInfinity(v1.X) ? -1 :
                    double.NaN;

                double y = v1.Y == 0 ? 0 :
                    v1.Y == -0 ? -0 :
                    double.IsPositiveInfinity(v1.Y) ? 1 :
                    double.IsNegativeInfinity(v1.Y) ? -1 :
                    double.NaN;

                double z = v1.Z == 0 ? 0 :
                    v1.Z == -0 ? -0 :
                    double.IsPositiveInfinity(v1.Z) ? 1 :
                    double.IsNegativeInfinity(v1.Z) ? -1 :
                    double.NaN;

                return new(x, y, z);
            }

            return v1;
        }

        #endregion
    }
}

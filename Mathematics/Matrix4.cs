﻿/**
 * @file Matrix4.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Matrix4 class.
 * @version 1.0
 * @date 25.01.2021
 *
 * @copyright CFPT (c) 2021
 *
 */
namespace POC3DViewer.Mathematics
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// Represents a matrix of size 4x4. Every coordinates are used in [y, x] format (or [row, column]).
    /// </summary>
    [ImmutableObject(true)]
    public struct Matrix4
        : IEquatable<Matrix4>
    {
        #region Constants

        /// <summary>
        /// Size of the matrix.
        /// </summary>
        public const int Size = 4;

        /// <summary>
        /// The identity matrix.
        /// </summary>
        public static readonly Matrix4 Identity = new(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);

        /// <summary>
        /// Matrix for projection onto the x = 0 plane.
        /// </summary>
        public static readonly Matrix4 ProjectionOrthoX = new(
            0, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);

        /// <summary>
        /// Matrix for projection onto the y = 0 plane.
        /// </summary>
        public static readonly Matrix4 ProjectionOrthoY = new(
            1, 0, 0, 0,
            0, 0, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);

        /// <summary>
        /// Matrix for projection onto the z = 0 plane.
        /// </summary>
        public static readonly Matrix4 ProjectionOrthoZ = new(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 0, 0,
            0, 0, 0, 1);

        /// <summary>
        /// The zero matrix.
        /// </summary>
        public static readonly Matrix4 Zero = new(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        #endregion

        private readonly double[,] values;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4"/> struct.
        /// </summary>
        /// <param name="values">Values of the matrix. Should be 4x4.</param>
        public Matrix4(double[,] values)
        {
            this.values = new double[Size, Size]
            {
                   { values[0, 0], values[0, 1], values[0, 2], values[0, 3] },
                   { values[1, 0], values[1, 1], values[1, 2], values[1, 3] },
                   { values[2, 0], values[2, 1], values[2, 2], values[2, 3] },
                   { values[3, 0], values[3, 1], values[3, 2], values[3, 3] },
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4"/> struct.
        /// </summary>
        /// <param name="value00">Value of the matrix in the 0, 0 position.</param>
        /// <param name="value01">Value of the matrix in the 0, 1 position.</param>
        /// <param name="value02">Value of the matrix in the 0, 2 position.</param>
        /// <param name="value03">Value of the matrix in the 0, 3 position.</param>
        /// <param name="value10">Value of the matrix in the 1, 0 position.</param>
        /// <param name="value11">Value of the matrix in the 1, 1 position.</param>
        /// <param name="value12">Value of the matrix in the 1, 2 position.</param>
        /// <param name="value13">Value of the matrix in the 1, 3 position.</param>
        /// <param name="value20">Value of the matrix in the 2, 0 position.</param>
        /// <param name="value21">Value of the matrix in the 2, 1 position.</param>
        /// <param name="value22">Value of the matrix in the 2, 2 position.</param>
        /// <param name="value23">Value of the matrix in the 2, 3 position.</param>
        /// <param name="value30">Value of the matrix in the 3, 0 position.</param>
        /// <param name="value31">Value of the matrix in the 3, 1 position.</param>
        /// <param name="value32">Value of the matrix in the 3, 2 position.</param>
        /// <param name="value33">Value of the matrix in the 3, 3 position.</param>
        [SuppressMessage(
            "StyleCop.CSharp.ReadabilityRules",
            "SA1117:Parameters should be on same line or separate lines",
            Justification = "For better readability of Matrix struct.")]
        public Matrix4(
            double value00, double value01, double value02, double value03,
            double value10, double value11, double value12, double value13,
            double value20, double value21, double value22, double value23,
            double value30, double value31, double value32, double value33) => values = new double[Size, Size]
            {
                { value00, value01, value02, value03 },
                { value10, value11, value12, value13 },
                { value20, value21, value22, value23 },
                { value30, value31, value32, value33 },
            };

        /// <summary>
        /// Initializes a new instance of the <see cref="Matrix4"/> struct.
        /// </summary>
        /// <param name="matrix">Matrix struct to take the values from.</param>
        public Matrix4(Matrix4 matrix) => values = matrix.values;

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets the values of the matrix.
        /// </summary>
        public double[,] Values
        {
            get
            {
                return new double[Size, Size]
                {
                    { values[0, 0], values[0, 1], values[0, 2], values[0, 3] },
                    { values[1, 0], values[1, 1], values[1, 2], values[1, 3] },
                    { values[2, 0], values[2, 1], values[2, 2], values[2, 3] },
                    { values[3, 0], values[3, 1], values[3, 2], values[3, 3] },
                };
            }
        }

        /// <summary>
        /// Gets the value at 0, 0 on the matrix.
        /// </summary>
        public double V00 => values[0, 0];

        /// <summary>
        /// Gets the value at 0, 1 on the matrix.
        /// </summary>
        public double V01 => values[0, 1];

        /// <summary>
        /// Gets the value at 0, 2 on the matrix.
        /// </summary>
        public double V02 => values[0, 2];

        /// <summary>
        /// Gets the value at 0, 3 on the matrix.
        /// </summary>
        public double V03 => values[0, 3];

        /// <summary>
        /// Gets the value at 1, 0 on the matrix.
        /// </summary>
        public double V10 => values[1, 0];

        /// <summary>
        /// Gets the value at 1, 1 on the matrix.
        /// </summary>
        public double V11 => values[1, 1];

        /// <summary>
        /// Gets the value at 1, 2 on the matrix.
        /// </summary>
        public double V12 => values[1, 2];

        /// <summary>
        /// Gets the value at 1, 3 on the matrix.
        /// </summary>
        public double V13 => values[1, 3];

        /// <summary>
        /// Gets the value at 2, 0 on the matrix.
        /// </summary>
        public double V20 => values[2, 0];

        /// <summary>
        /// Gets the value at 2, 1 on the matrix.
        /// </summary>
        public double V21 => values[2, 1];

        /// <summary>
        /// Gets the value at 2, 2 on the matrix.
        /// </summary>
        public double V22 => values[2, 2];

        /// <summary>
        /// Gets the value at 2, 3 on the matrix.
        /// </summary>
        public double V23 => values[2, 3];

        /// <summary>
        /// Gets the value at 2, 0 on the matrix.
        /// </summary>
        public double V30 => values[3, 0];

        /// <summary>
        /// Gets the value at 2, 1 on the matrix.
        /// </summary>
        public double V31 => values[3, 1];

        /// <summary>
        /// Gets the value at 2, 2 on the matrix.
        /// </summary>
        public double V32 => values[3, 2];

        /// <summary>
        /// Gets the value at 2, 3 on the matrix.
        /// </summary>
        public double V33 => values[3, 3];

        /// <summary>
        /// Gets the determinant of the matrix.
        /// </summary>
        public double Determinant
        {
            get
            {
                return (V00 * V11 * V22 * V33) -
                    (V00 * V11 * V23 * V32) +
                    (V00 * V12 * V23 * V31) -
                    (V00 * V12 * V21 * V33) +
                    (V00 * V13 * V21 * V32) -
                    (V00 * V13 * V22 * V31) -
                    (V01 * V12 * V23 * V30) +
                    (V01 * V12 * V20 * V33) -
                    (V01 * V13 * V20 * V32) +
                    (V01 * V13 * V22 * V30) -
                    (V01 * V10 * V22 * V33) +
                    (V01 * V10 * V23 * V32) +

                    (V02 * V13 * V20 * V31) -
                    (V02 * V13 * V21 * V30) +
                    (V02 * V10 * V21 * V33) -
                    (V02 * V10 * V23 * V31) +
                    (V02 * V11 * V23 * V30) +
                    (V02 * V11 * V20 * V33) -
                    (V03 * V10 * V21 * V32) +
                    (V03 * V10 * V22 * V31) -
                    (V03 * V11 * V22 * V30) +
                    (V03 * V11 * V20 * V32) -
                    (V03 * V12 * V20 * V31) +
                    (V03 * V12 * V21 * V30);
            }
        }

        /// <summary>
        /// Gets the normalized matrix.
        /// </summary>
        public Matrix4 Normalized => Normalize(this);

        /// <summary>
        /// Gets the trace of the matrix, the sum of the values along the diagonal axis.
        /// </summary>
        public double Trace => V00 + V11 + V22 + V33;

        #endregion Properties

        #region Operators

        /// <summary>
        /// Gets the value at the specified row and column.
        /// </summary>
        /// <param name="rowIndex">The index of the row.</param>
        /// <param name="columnIndex">The index of the column.</param>
        /// <returns>The element at the given row and column index.</returns>
        public double this[int rowIndex, int columnIndex] => values[rowIndex, columnIndex];

        /// <summary>
        /// Adds a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to add to.</param>
        /// <param name="scalar">Scalar to add to the matrix.</param>
        /// <returns>The added matrix.</returns>
        public static Matrix4 operator +(Matrix4 mat, double scalar)
        {
            double[,] newMat = new double[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMat[i, j] = mat[i, j] + scalar;
                }
            }

            return new(newMat);
        }

        /// <summary>
        /// Adds a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to add to.</param>
        /// <param name="scalar">Scalar to add to the matrix.</param>
        /// <returns>The added matrix.</returns>
        public static Matrix4 operator +(double scalar, Matrix4 mat) => mat + scalar;

        /// <summary>
        /// Substracts a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to substract to.</param>
        /// <param name="scalar">Scalar to substract to the matrix.</param>
        /// <returns>The substracted matrix.</returns>
        public static Matrix4 operator -(Matrix4 mat, double scalar)
        {
            double[,] newMat = new double[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMat[i, j] = mat[i, j] - scalar;
                }
            }

            return new(newMat);
        }

        /// <summary>
        /// Substracts a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to substract to.</param>
        /// <param name="scalar">Scalar to substract to the matrix.</param>
        /// <returns>The substracted matrix.</returns>
        public static Matrix4 operator -(double scalar, Matrix4 mat) => mat - scalar;

        /// <summary>
        /// Multiplies a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to multiply.</param>
        /// <param name="scalar">Scalar to multiply with the matrix.</param>
        /// <returns>The multiplied matrix.</returns>
        public static Matrix4 operator *(Matrix4 mat, double scalar)
        {
            double[,] newMat = new double[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMat[i, j] = mat[i, j] * scalar;
                }
            }

            return new(newMat);
        }

        /// <summary>
        /// Multiplies a matrix and a scalar.
        /// </summary>
        /// <param name="mat">Matrix to multiply.</param>
        /// <param name="scalar">Scalar to multiply with the matrix.</param>
        /// <returns>The multiplied matrix.</returns>
        public static Matrix4 operator *(double scalar, Matrix4 mat) => mat * scalar;

        /// <summary>
        /// Divides a matrix by a scalar.
        /// </summary>
        /// <param name="mat">Matrix to divide.</param>
        /// <param name="scalar">Scalar to divide the matrix with.</param>
        /// <returns>The divided matrix.</returns>
        public static Matrix4 operator /(Matrix4 mat, double scalar)
        {
            double[,] newMat = new double[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMat[i, j] = mat[i, j] / scalar;
                }
            }

            return new(newMat);
        }

        /// <summary>
        /// Inverses the sign of a matrix.
        /// </summary>
        /// <param name="mat">Matrix to inverse the sign of.</param>
        /// <returns>The sign inverted matrix.</returns>
        public static Matrix4 operator -(Matrix4 mat)
        {
            double[,] newMat = new double[Size, Size]
            {
                { -mat.V00, -mat.V01, -mat.V02, -mat.V03 },
                { -mat.V10, -mat.V11, -mat.V12, -mat.V13 },
                { -mat.V20, -mat.V21, -mat.V22, -mat.V23 },
                { -mat.V30, -mat.V31, -mat.V32, -mat.V33 },
            };

            return new(newMat);
        }

        /// <summary>
        /// Adds two matrices.
        /// </summary>
        /// <param name="left">The matrix on the left.</param>
        /// <param name="right">The matrix on the right.</param>
        /// <returns>The added matrix.</returns>
        public static Matrix4 operator +(Matrix4 left, Matrix4 right)
        {
            double[,] newMat = new double[Size, Size];

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMat[i, j] = left[i, j] + right[i, j];
                }
            }

            return new(newMat);
        }

        /// <summary>
        /// Substracts two matrices.
        /// </summary>
        /// <param name="left">The matrix on the left.</param>
        /// <param name="right">The matrix on the right.</param>
        /// <returns>The substracted matrix.</returns>
        public static Matrix4 operator -(Matrix4 left, Matrix4 right)
        {
            double[,] newMat = new double[Size, Size];

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMat[i, j] = left[i, j] - right[i, j];
                }
            }

            return new(newMat);
        }

        /// <summary>
        /// Multiplies two matrices.
        /// </summary>
        /// <param name="left">The left matrix.</param>
        /// <param name="right">The right matrix.</param>
        /// <returns>The dot product of the matrices.</returns>
        public static Matrix4 operator *(Matrix4 left, Matrix4 right)
        {
            double[,] newMat = new double[Size, Size];

            newMat[0, 0] = (left.V00 * right.V00) + (left.V01 * right.V10) + (left.V02 * right.V20) + (left.V03 * right.V30);
            newMat[0, 1] = (left.V00 * right.V01) + (left.V01 * right.V11) + (left.V02 * right.V21) + (left.V03 * right.V31);
            newMat[0, 2] = (left.V00 * right.V02) + (left.V01 * right.V12) + (left.V02 * right.V22) + (left.V03 * right.V32);

            newMat[1, 0] = (left.V10 * right.V00) + (left.V11 * right.V10) + (left.V12 * right.V20) + (left.V13 * right.V30);
            newMat[1, 1] = (left.V10 * right.V01) + (left.V11 * right.V11) + (left.V12 * right.V21) + (left.V13 * right.V31);
            newMat[1, 2] = (left.V10 * right.V02) + (left.V11 * right.V12) + (left.V12 * right.V22) + (left.V13 * right.V32);

            newMat[2, 0] = (left.V20 * right.V00) + (left.V21 * right.V10) + (left.V22 * right.V20) + (left.V23 * right.V30);
            newMat[2, 1] = (left.V20 * right.V01) + (left.V21 * right.V11) + (left.V22 * right.V21) + (left.V23 * right.V31);
            newMat[2, 2] = (left.V20 * right.V02) + (left.V21 * right.V12) + (left.V22 * right.V22) + (left.V23 * right.V32);

            newMat[3, 0] = (left.V30 * right.V00) + (left.V31 * right.V10) + (left.V32 * right.V20) + (left.V33 * right.V30);
            newMat[3, 1] = (left.V30 * right.V01) + (left.V31 * right.V11) + (left.V32 * right.V21) + (left.V33 * right.V31);
            newMat[3, 2] = (left.V30 * right.V02) + (left.V31 * right.V12) + (left.V32 * right.V22) + (left.V33 * right.V32);

            return new(newMat);
        }

        /// <summary>
        /// Check if two matrices are equal.
        /// </summary>
        /// <param name="left">The left matrix.</param>
        /// <param name="right">The right matrix.</param>
        /// <returns>True if the matrices are equal.</returns>
        public static bool operator ==(Matrix4 left, Matrix4 right) => left.Equals(right);

        /// <summary>
        /// Check if two matrices are not equal.
        /// </summary>
        /// <param name="left">The left matrix.</param>
        /// <param name="right">The right matrix.</param>
        /// <returns>True if the matrices are not equal.</returns>
        public static bool operator !=(Matrix4 left, Matrix4 right) => !(left == right);

        #endregion Operators

        #region Methods

        /// <summary>
        /// Builds a rotation matrix for a rotation around x-axis.
        /// </summary>
        /// <param name="angle">The counter clockwise angle in radian.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4 CreateRotationX(double angle)
        {
            return new Matrix4(new double[,]
            {
                { 1, 0, 0, 0 },
                { 0, Math.Cos(angle), Math.Sin(angle), 0 },
                { 0, -Math.Sin(angle), Math.Cos(angle), 0 },
                { 0, 0, 0, 1 },
            });
        }

        /// <summary>
        /// Builds a rotation matrix for a rotation around y-axis.
        /// </summary>
        /// <param name="angle">The counter clockwise angle in radian.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4 CreateRotationY(double angle)
        {
            return new Matrix4(new double[,]
            {
                { Math.Cos(angle), 0, -Math.Sin(angle), 0 },
                { 0, 1, 0, 0 },
                { Math.Sin(angle), 0, Math.Cos(angle), 0 },
                { 0, 0, 0, 1 },
            });
        }

        /// <summary>
        /// Builds a rotation matrix for a rotation around z-axis.
        /// </summary>
        /// <param name="angle">The counter clockwise angle in radian.</param>
        /// <returns>The rotation matrix.</returns>
        public static Matrix4 CreateRotationZ(double angle)
        {
            return new Matrix4(new double[,]
            {
                { Math.Cos(angle), Math.Sin(angle), 0, 0 },
                { -Math.Sin(angle), Math.Cos(angle), 0, 0 },
                { 0, 0, 1, 0 },
                { 0, 0, 0, 1 },
            });
        }

        /// <summary>
        /// Creates an perspective projection matrix.
        /// </summary>
        /// <param name="left">Left edge of the view frustum.</param>
        /// <param name="right">Right edge of the view frustum.</param>
        /// <param name="bottom">Bottom edge of the view frustum.</param>
        /// <param name="top">Top edge of the view frustum.</param>
        /// <param name="depthNear">Distance to the near clip plane.</param>
        /// <param name="depthFar">Distance to the far clip plane.</param>
        /// <returns>A perspective projection matrix.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown under the following conditions:
        ///  <list type="bullet">
        ///  <item>depthNear is negative or zero</item>
        ///  <item>depthFar is negative or zero</item>
        ///  <item>depthNear is larger than depthFar</item>
        ///  </list>
        /// </exception>
        /// <remarks>Taken from here : https://github.com/opentk/opentk/blob/082c8d228d0def042b11424ac002776432f44f47/src/OpenTK.Mathematics/Matrix/Matrix4d.cs#L1024. </remarks>
        public static Matrix4 CreatePerspectiveOffCenter(
            double left,
            double right,
            double bottom,
            double top,
            double depthNear,
            double depthFar)
        {
            if (depthNear <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(depthNear));
            }

            if (depthFar <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(depthFar));
            }

            if (depthNear >= depthFar)
            {
                throw new ArgumentOutOfRangeException(nameof(depthNear));
            }

            double x = 2.0f * depthNear / (right - left);
            double y = 2.0f * depthNear / (top - bottom);
            double a = (right + left) / (right - left);
            double b = (top + bottom) / (top - bottom);
            double c = -(depthFar + depthNear) / (depthFar - depthNear);
            double d = -(2.0f * depthFar * depthNear) / (depthFar - depthNear);

#pragma warning disable SA1117 // Parameters should be on same line or separate lines
            return new Matrix4(
                x, 0, a, 0,
                0, y, b, 0,
                0, 0, c, d,
                0, 0, -1, 0);
#pragma warning restore SA1117 // Parameters should be on same line or separate lines
        }

        /// <summary>
        /// Creates a perspective projection matrix.
        /// </summary>
        /// <param name="fovy">Angle of the field of view in the y direction (in radians).</param>
        /// <param name="aspect">Aspect ratio of the view (width / height).</param>
        /// <param name="depthNear">Distance to the near clip plane.</param>
        /// <param name="depthFar">Distance to the far clip plane.</param>
        /// <returns>A perspective projection matrix.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown under the following conditions:
        ///  <list type="bullet">
        ///  <item>fovy is zero, less than zero or larger than Math.PI</item>
        ///  <item>aspect is negative or zero</item>
        ///  <item>depthNear is negative or zero</item>
        ///  <item>depthFar is negative or zero</item>
        ///  <item>depthNear is larger than depthFar</item>
        ///  </list>
        /// </exception>
        public static Matrix4 CreatePerspectiveFieldOfView(double fovy, double aspect, double depthNear, double depthFar)
        {
            if (fovy <= 0 || fovy > Math.PI)
            {
                throw new ArgumentOutOfRangeException(nameof(fovy));
            }

            if (aspect <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(aspect));
            }

            if (depthNear <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(depthNear));
            }

            if (depthFar <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(depthFar));
            }

            double maxY = depthNear * Math.Tan(0.5 * fovy);
            double minY = -maxY;
            double minX = minY * aspect;
            double maxX = maxY * aspect;

            return CreatePerspectiveOffCenter(minX, maxX, minY, maxY, depthNear, depthFar);
        }

        /// <summary>
        /// Computes the transpose of the given matrix.
        /// </summary>
        /// <param name="mat">Matrix to compute the transpose of.</param>
        /// <returns>The transpose of the given matrix.</returns>
        public static Matrix4 Transpose(Matrix4 mat)
        {
            double[,] newMat = new double[Size, Size];

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    newMat[j, i] = mat[i, j];
                }
            }

            return new(newMat);
        }

        /// <summary>
        /// Divides each element in the Matrix by the <see cref="Determinant"/>.
        /// </summary>
        /// <param name="mat">Matrix to normalize.</param>
        /// <returns>The normalized matrix.</returns>
        public static Matrix4 Normalize(Matrix4 mat)
        {
            double determinant = mat.Determinant;
            return mat / determinant;
        }

        /// <summary>
        /// Computes the transpose of this matrix.
        /// </summary>
        /// <returns>The transpose of this matrix.</returns>
        public Matrix4 Transpose() => Transpose(this);

        /// <inheritdoc/>
        public override bool Equals(object other)
        {
            // Check if the other object is a Matrix4
            if (other is Matrix4 mat)
            {
                return mat.Equals(this);
            }

            return false;
        }

        /// <inheritdoc/>
        public bool Equals(Matrix4 other)
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (values[i, j] != other.values[i, j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Comparator withing a tolerance.
        /// </summary>
        /// <param name="other">Matrix to compare with.</param>
        /// <param name="tolerance">Tolerance to apply in the comparison.</param>
        /// <returns>True if two matrices are equal withing a tolerance.</returns>
        public bool Equals(object other, double tolerance)
        {
            // Check if the other object is a Matrix4
            if (other is Matrix4 mat)
            {
                return mat.Equals(this, tolerance);
            }

            return false;
        }

        /// <summary>
        /// Comparator withing a tolerance.
        /// </summary>
        /// <param name="other">Matrix to compare with.</param>
        /// <param name="tolerance">Tolerance to apply in the comparison.</param>
        /// <returns>True if two matrices are equal withing a tolerance.</returns>
        public bool Equals(Matrix4 other, double tolerance)
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (values[i, j].AlmostEqualsWithAbsTolerance(other.values[i, j], tolerance))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <inheritdoc/>
        public override int GetHashCode() => HashCode.Combine(values);
        #endregion
    }
}